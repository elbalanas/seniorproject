import numpy as np
import time
#def checkConverge(V,V_old):
#    totalDif=np.sum(np.absolute(V-V_old))
#    totalOld=np.sum(np.absolute(V_old))
#    return totalDif < (0.001*totalOld)

def policy_v(P,gamma,R):
    
    start_time = time.time()
    count=0
    pol = [0]*len(P)
    V = [0] * len(P)
    converged = False
    P_V=np.array(P)
    V_V=np.array(V)
    R_V=np.array(R)
    pol_V=np.array(pol)
    
    while(count!=2):
        count+=1
        V_old_V=V_V
        Sum_V=np.multiply(gamma,P_V.dot(V_V))
        print Sum_V
        futureVal_V=np.amax(Sum_V,axis=1)
        pol_V=np.argmax(Sum_V,axis=1)
        V_V=R_V+futureVal_V
        print V_V
        print V_old_V
        train_time = time.time() - start_time
        print '--- %s seconds ---%s loops---' % (train_time,count)
    return pol

#import time
#
#def checkConverge(new,old):
#  totalDif = 0
#  totalOld = 0
#  for i in range(0,len(old)):
#    totalDif += abs(new[i] - old[i])
#    totalOld += abs(old[i])
#  return (totalDif < 0.001*totalOld)


#S(states) and A(actions) are implicitly integers implicitly defined by P
#(P[state][action][state_] = p(state->state_|action)
def policy(P,gamma,R):
    start_time = time.time()
    
    pol = [0]*len(P)
    V = [0] * len(P)
    converged = False
    count=0

    count+=1
    V_ = V[:] #track previous iteration for comparison
    #iterate over each state
    print "1loop"
    
    for s in range(0,len(P)):
      #print len(P),"outer loop",s
      futureVal = -float('Inf')
      #iterate over each action
      for a in range(0,len(P[s])):
        #print len(P[s]),"middle loop",a
        arg = 0
        val = 0
        #iterate over each destination state
        for s_ in range(0,len(P[s][a])):
          print
          val += (gamma*(P[s][a][s_] * V[s_]))
          print val
        print "---",val
        if (val > futureVal):
          futureVal = val
          pol[s] = a
      #print V
      V[s] = R[s] + futureVal
    
  
    
    train_time = time.time() - start_time
    print '--- %s seconds ---%s loops---' % (train_time,count)
    return pol

P=[[[1,1,1],[2,2,2]],[[3,3,3],[4,4,4]],[[5,5,5],[6,6,6]]]
gamma=0.5
R=[1,2,3]

print policy_v(P,gamma,R)
P=[[[1,1,1],[2,2,2]],[[3,3,3],[4,4,4]],[[5,5,5],[6,6,6]]]
gamma=0.5
R=[1,2,3]
print policy(P,gamma,R)
