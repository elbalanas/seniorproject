import numpy as np
import time
def checkConverge(V,V_old):
    totalDif=np.sum(np.absolute(V-V_old))
    totalOld=np.sum(np.absolute(V_old))
    return totalDif < (0.001*totalOld)

def policy(P,gamma,R):
    
    start_time = time.time()
    count=0
    pol = [0]*len(P)
    V = [0] * len(P)
    converged = False
    P_V=np.array(P)
    V_V=np.array(V)
    R_V=np.array(R)
    pol_V=np.array(pol)
    
    while not (converged):
        count+=1
        V_old_V=V_V
        Sum_V=np.multiply(gamma,P_V.dot(V_V))
        futureVal_V=np.amax(Sum_V,axis=1)
        pol_V=np.argmax(Sum_V,axis=1)
        V_V=R_V+futureVal_V
        converged = checkConverge(V_V, V_old_V)
    train_time = time.time() - start_time
    print 'Triaining Time %s seconds with %s loops' % (train_time,count)
    return pol_V