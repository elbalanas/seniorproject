"""The start screen for the game"""

import random
import os
import time
import pygame

import serge.actor
import serge.visual
import serge.events
import serge.common
import serge.sound
from serge.simplevecs import Vec2d
import serge.blocks.utils
import serge.blocks.visualblocks
import serge.blocks.behaviours
import serge.blocks.actors
import serge.blocks.animations

from theme import G, theme
import common
import smacktalker
import powerups
#NOTE BM
import logging
import json

ENTERED = ["ENTER YOUR NAME"]
first = True
#END BM
class StartScreen(serge.blocks.actors.ScreenActor):
    """The logic for the start screen"""

    def __init__(self, options):
        """Initialise the screen"""
        super(StartScreen, self).__init__('item', 'main-screen')
        self.options = options
        self._take_screenshots = G('auto-screenshots')
        self._screenshot_interval = G('screenshot-interval')
        self._last_screenshot = time.time() - self._screenshot_interval + 1.0
        self._screenshot_path = G('screenshot-path')
        self.music = common.MAIN_MUSIC = serge.sound.Music.getItem('titles')
        serge.sound.Music.setVolume(G('volume', 'start-screen'))
        self.music.play(-1)

    def addedToWorld(self, world):
        """The start screen was added to the world"""
        super(StartScreen, self).addedToWorld(world)
        #
        # Logo
        the_theme = theme.getTheme('start-screen')
        L = the_theme.getProperty
        logo = serge.blocks.utils.addSpriteActorToWorld(world, 'logo', 'logo', 'logo', 'foreground',
            center_position=L('logo-position'))
        title = serge.blocks.utils.addSpriteActorToWorld(world, 'logo', 'title', 'title', 'foreground',
            center_position=L('title-position'))
        bg = serge.blocks.utils.addSpriteActorToWorld(
            world, 'bg', 'bg', 'main-background',
            layer_name='background',
            center_position=(G('screen-width') / 2, G('screen-height') / 2),
        )
        #
        # Face
        self.face = serge.blocks.utils.addSpriteActorToWorld(
            world, 'face', 'face', 'face',
            layer_name='ui',
            center_position=L('face-position'),
        )
        self.face.linkEvent(serge.events.E_LEFT_CLICK, common.tweenWorlds('level-screen'))
        #
        self.start_text=serge.blocks.utils.addActorToWorld(
            world,
            serge.blocks.actors.FormattedText(
                'score', 'ai-score',
                'Press Enter to start',
                G('player-colour'),
                font_name=G('player-font'), font_size=G('player-font-size'),
                justify='left', fixed_char_width=20,
                Name="",
            ),
            layer_name='ui',
            center_position=(200,450)
        )
#        start = serge.blocks.utils.addSpriteActorToWorld(world, 'logo', 'start', 'start', 'foreground',
#            center_position=L('start-position'))
        #start.linkEvent(serge.events.E_LEFT_CLICK, common.tweenWorlds('level-screen'))
        credits = serge.blocks.utils.addSpriteActorToWorld(world, 'logo', 'credits', 'credits', 'foreground',
            center_position=L('credits-position'))
        credits.linkEvent(serge.events.E_LEFT_CLICK, common.tweenWorlds('credits-screen'))
        help = serge.blocks.utils.addSpriteActorToWorld(world, 'logo', 'help', 'help', 'foreground',
            center_position=L('help-position'))
        help.linkEvent(serge.events.E_LEFT_CLICK, common.tweenWorlds('help-screen'))
        achievements = serge.blocks.utils.addSpriteActorToWorld(world, 'logo', 'achievements', 'achievements', 'foreground',
            center_position=L('achievements-position'))
        achievements.linkEvent(serge.events.E_LEFT_CLICK, common.tweenWorlds('achievements-screen'))
        achievements.active = False
        #
        serge.blocks.utils.addTextItemsToWorld(world, [
                    ('v' + common.version, 'version'),
                ],
                the_theme, 'foreground'
        )
        #
        #NOTE BM
        self.test_text=serge.blocks.utils.addActorToWorld(
            world,
            serge.blocks.actors.FormattedText(
                'score', 'ai-score',
                'Name : "%(Name)s"',
                (255, 255, 150),
                'DEFAULT', font_size=G('player-font-size'),
                justify='left', fixed_char_width=25,
                Name="ENTER YOUR NAME",
            ),
            layer_name='ui',
            center_position=(150,150)
        )
        # Sprite to show items that appear for the AI to comment on
#        self.appearing_item = serge.blocks.utils.addSpriteActorToWorld(
#            world, 'item', 'appearing-item', 'tiles-31',
#            center_position=L('item-start-position'),
#            layer_name='ui',
#            actor_class=serge.blocks.animations.AnimatedActor
#        )
#        self.appearing_item.setZoom(L('item-zoom'))
#        items = list(set([getattr(powerups, name) for name in G('random-item-names')]))
#        random.shuffle(items)
#        self.items = items
#        #
#        # Smack talking
#        self.smack = smacktalker.RandomlyAppearingSmacker('smack', 'smack', 'start-screen', 'TOBEREPLACED')
#        self.setSmackConversation()
#        world.addActor(self.smack)
#        self.smack.visible = False
#        self.smack.linkEvent(common.E_SMACK_APPEAR, self.showItem)
#        self.smack.linkEvent(common.E_SMACK_HIDE, self.hideItem)
        #END BM
    def updateActor(self, interval, world):
        """Update this actor"""
        super(StartScreen, self).updateActor(interval, world)
        #
        global ENTERED
        global first
        if self.keyboard.areAnyClicked():
            #logging.warning("--------------------------")
            #logging.warning(self.keyboard.getClicked()[0])
            if self.keyboard.getClicked()[0] == pygame.K_BACKSPACE :
                if first == True or len(ENTERED)==0:
                    return
#                logging.warning('erase')
                ENTERED.pop();
#                logging.warning(''.join(chr(i) for i in ENTERED))
                temp=''.join(chr(i) for i in ENTERED)
                self.test_text.setValue('Name',temp)
            elif self.keyboard.getClicked()[0] == pygame.K_RETURN :
                if first == True:
                    return
#                logging.warning('save')
#                NOTE BM
#                Test write read file with json
#                logging.warning("show content in file")
#                x=[]
#                if os.path.isfile("brain/current_user.json") :
#                    f = open('brain/current_user.json', 'r')
#                    x=json.load(f)
#                    #logging.warning(x)
#                    f.close();
                #print(json.dumps([1, 'simple', 'list']))
                #f.write('0123456789abcdef') 
                #json.dumps([1, 'simple', 'list'],f)
                f = open(G('save-path-session')+'current_user.json', 'w')
                #f.write(json.dumps([{1:"a"},{2:"c"}]))
#                x.append(''.join(chr(i) for i in ENTERED))
                x={'username':''.join(chr(i) for i in ENTERED),'time':time.time()}
                
#                logging.warning(x)
                f.write(json.dumps(x))
                #f.write(x.append(''.join(chr(i) for i in ENTERED)))
                f.close();
                
                #END BM
            elif self.keyboard.getClicked()[0] >= 256 :                 
                #logging.warning(self.keyboard.getClicked()[0])
                logging.warning("type charactor out off range")
            else :
                if first == True:
                    ENTERED=[]
                    first = False
                ENTERED.append(self.keyboard.getClicked()[0])
                #ENTERED = self.keyboard.getClicked()
#                logging.warning(''.join(chr(i) for i in ENTERED))
                #logging.warning(pygame.K_RETURN)
                temp=''.join(chr(i) for i in ENTERED)
                self.test_text.setValue('Name',temp)
                #logging.warning(self.keyboard.getClicked()[0])
        # Keypresses
        if self.keyboard.isClicked(pygame.K_RETURN):
            self.engine.setCurrentWorldByName('level-screen')
        #
        # Face update
        if random.random() * 1000.0 / interval < G('face-probability', 'start-screen'):
            self.face.visual.setCell(random.randint(1, self.face.visual.getNumberOfCells() - 1))
        if self._take_screenshots:
            if time.time() - self._last_screenshot > self._screenshot_interval:
                filename = '%s-%s' % (self.name, time.strftime('%m-%d %H:%M:%S.png'))
                serge.blocks.utils.takeScreenshot(os.path.join(self._screenshot_path, filename))
                self._last_screenshot = time.time()
                self.log.debug('Taking screenshot - %s', filename)
        

    def showItem(self, obj, arg):
        """Show the random item"""
        self.log.debug('Showing the random item')
        the_theme = theme.getTheme('start-screen')
        L = the_theme.getProperty
        self.zoom_in = self.appearing_item.addAnimation(
            serge.blocks.animations.MovementTweenAnimation(
                self.appearing_item, Vec2d(L('item-start-position')), Vec2d(L('item-end-position')),
                duration=L('item-animation-time'),
                function=serge.blocks.animations.MovementTweenAnimation.sinInOut,
                set_immediately=True,
            ),
            'item-enter',
        )
        #
        self.appearing_item.setSpriteName(self.items[0].name_of_sprite)
        self.items.append(self.items.pop(0))
        self.setSmackConversation()

    def setSmackConversation(self):
        """Set the smack conversation"""
        self.smack.conversation = 'show-%s' % self.items[0].__name__.lower()
        logging.warning(self.items)

    def hideItem(self, obj, arg):
        """Show the random item"""
        self.log.debug('Hiding the random item')
        the_theme = theme.getTheme('start-screen')
        L = the_theme.getProperty
        self.zoom_out = self.appearing_item.addAnimation(
            serge.blocks.animations.MovementTweenAnimation(
                self.appearing_item, Vec2d(L('item-end-position')), Vec2d(L('item-start-position')),
                duration=L('item-animation-time'),
                function=serge.blocks.animations.MovementTweenAnimation.sinInOut,
                set_immediately=False,
            ),
            'item-leave',
        )


def main(options):
    """Create the main logic"""
    #
    # The screen actor
    s = StartScreen(options)
    world = serge.engine.CurrentEngine().getWorld('start-screen')
    world.addActor(s)
    #
    # The behaviour manager
    manager = serge.blocks.behaviours.BehaviourManager('behaviours', 'behaviours')
    world.addActor(manager)
    manager.assignBehaviour(None, serge.blocks.behaviours.KeyboardQuit(), 'keyboard-quit')
    #
    # Screenshots
    if options.screenshot:
        manager.assignBehaviour(None,
            serge.blocks.behaviours.SnapshotOnKey(key=pygame.K_s, size=G('screenshot-size')
                , overwrite=False, location='screenshots'), 'screenshots')

