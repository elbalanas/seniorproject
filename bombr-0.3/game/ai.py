#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Controller for the AI"""

import random
from functools import partial
from collections import defaultdict

import numpy as np
import math

import serge.actor
import serge.common
import serge.sound
import serge.blocks.directions
import serge.blocks.fysom
import serge.blocks.utils
import serge.blocks.visualblocks
import serge.blocks.settings
from theme import G, theme

from peas.methods.neat import NEATPopulation, NEATGenotype
from peas.networks.rnn import NeuralNetwork

from board import NoPath

import bomb
import powerups
import logging
import time
import pickle

import dill
import os
import json

from sklearn.decomposition import PCA
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC

import reinforce as l

# States
S_WAITING = 'waiting'
S_MOVING = 'moving'
S_ESCAPING = 'escaping'
S_CLEARING = 'clearing'
S_MOVING_FOR_KILL = 'moving-for-kill'

# Events
E_ARRIVED = 'arrived'
E_CHOSE_ATTACK = 'chose_attack'
E_CHOSE_ESCAPE = 'chose_escape'
E_CHOSE_CLEAR = 'chose_clear'
E_DROP_BOMB = 'drop_bomb'
E_ENDANGERED = 'endangered'
E_KILL_SPOTTED = 'kill_spotted'

# Strategies
X_CLOSE_BY = 'close-by'
X_FAR_FROM = 'far-from'
X_PLAYER = 'player'
X_HEART = 'heart'
X_FLAG = 'flag'



class predictorTask(object):

    """This Class used to be part of original algor NEAT as its task
    But we didnot use NEAT anymore so this part is unused 
    However we will keep it together with NEAT just in case"""

    # I/O Example
    # INPUTS  = [(0,0), (0,1), (1,0), (1,1)]
    # OUTPUTS = [(-1,), (1,), (1,), (-1,)]

    EPSILON = 1e-100
    inputs = []
    outputs = []


    def __init__(self,do_all=True,match_input=None,match_output=None):
        self.do_all = do_all
        self.inputs = np.array(match_input, dtype=float)
        self.outputs = np.array(match_output, dtype=float)


    def evaluate(self, network, verbose=False):

        # if network is not neuralNetwork change it to Neural
        if not isinstance(network, NeuralNetwork):
            network = NeuralNetwork(network)

        network.make_feedforward()

        # zip output and input together -> dont need this if we do reinforcement
        pairs = zip(self.inputs, self.outputs)
        random.shuffle(pairs)

        if not self.do_all:
            pairs = [random.choice(pairs)]

        rmse = 0.0

        # run evaluating test on every input/output pairs
        for (i, target) in pairs:

            output = network.feed(i)
            output = output[-len(target):]

            # clip output with softmax so we got propability
            outputA = np.exp(output) / np.sum(np.exp(output), axis=0)

            # entropy for softmax
            ans = np.log(outputA)
            err = -np.sum(target * ans)

            if verbose:
                print '%r -> %r (%.2f)' % (i, output, err)
           
            rmse += err

        score = 1 / (1 + rmse / len(pairs))
        return {'fitness': score}


    def solve(self, network):
        return int(self.evaluate(network) > 0.6)




class AI(serge.common.Loggable):

    """Represents the AI's control of the actor"""

    long_term_memory = {}

    def __init__(self, enemy=None):
        """Initialise the AI"""

        self.addLogger()
        
        self.enemy = enemy

        self._last_move = 0.0
        self._total_time = 0.0
        self.move_interval = G('ai-move-interval')
        
        self.selected_path = None
        self.recently_stategy=None
        self.recently_state=None

        self.state = serge.blocks.fysom.Fysom({
            'initial': S_WAITING,
            'events': [
                {'name': E_ARRIVED,
                 'src': [S_MOVING, S_CLEARING, S_ESCAPING],
                 'dst': S_WAITING,
                 },
                {'name': E_DROP_BOMB,
                 'src': [S_MOVING, S_CLEARING, S_ESCAPING, S_WAITING],
                 'dst': S_ESCAPING,
                 },
                {'name': E_ENDANGERED,
                 'src': [S_MOVING, S_CLEARING, S_WAITING, S_ESCAPING],
                 'dst': S_ESCAPING,
                 },
                {'name': E_KILL_SPOTTED,
                 'src': [S_WAITING],
                 'dst': S_MOVING_FOR_KILL,
                 },
            ],
            'callbacks': {
                'onarrived': self.onArrived,
                'onendangered': self.onEndangered,
            }
        })

        self.walk = serge.sound.Sounds.getItem('walk')
        self.enemy_position = {'enemy_x': 3, 'enemy_y': 3}
        self.heart_grab_distance = G('heart-grab-distance')

        # how many time u has been play this game
        self.playRound = None

        # input_mem and output_mem are used to stagnate information before feed it to network
        self.input_mem = []
        self.output_mem = []

        # these 2 are used in predict function to find the true result of prediction
        self.predict_true = 0.0
        self.predict_false = 0.0

        # pca and brain are what we save in our pickle -> load everytime when the game start to play
        self.pca = None
        self.brain = None
        self.train_time = 0

        # word of the smallest action of ai -> unused -> 
        self.words = ['left', 'right', 'up', 'down', 'bomb', 'wait', ]
        
        # precise and recall evaluation
        self.truePositive = {key: value for (key, value) in zip(self.words,[0,0,0,0,0,0])}
        self.falseNegative = {key: value for (key, value) in zip(self.words,[0,0,0,0,0,0])}
        self.falsePositive = {key: value for (key, value) in zip(self.words,[0,0,0,0,0,0])}
        
        # for decision making
        self.stategy= None
        self.myActionHistory=[]
        self.setupAi=False

        # TD learning
        self.TDTrainingData = []
        self.TDStateDict = {}
        
        # action strategy we have
        self.actions=["red_heart","green_heart","flag","clearing","shortest","worth","aggressive"]
       

    def traslateOutputBinaryToWord(self, result):
        return self.words[result]


    def predict(self, feature, target):
        """
            Predict Oppoment movement by learning from their movement pattern in the past
            originally aim to use in real person simulation -> simulation bug so doesn't use this now
        """

        # Predit by NEAT
        # predic_result=self.getPredictResultNEAT(feature)

        # Predit by NeuralNetwork
        predic_result = self.getPredictResultNeural(feature)

        # take predict action from predict list eg.[0,0,0,1,0]
        targetx = np.argmax(target)
        predic_result = self.traslateOutputBinaryToWord(predic_result)
        targetx = self.traslateOutputBinaryToWord(targetx)

        # save temp static to make a report on end game
        self.updateResultPredict(predic_result, targetx)


    def getPredictResultNeural(self, feature):
        """
            Predict Function using standard Neural network from library
            Used in predict function
        """
        network = self.brain

        # with PCA
        # output=network.predict(feature.reshape(1,-1))

        # with out PCA
        output = network.predict(np.array(feature).reshape(1, -1))

        return output[0]


    def getPredictResultNEAT(self, feature):
        """
            Predict Function using NEAT Algorithm
            Used in predict function
        """

        # set network to last champions
        network = self.brain.champions[-1]
        
        if not isinstance(network, NeuralNetwork):
            network = NeuralNetwork(network)
        
        network.make_feedforward()
        output = network.feed(feature)
        output = output[-6:]

        # clip output with softmax
        output = np.exp(output) / np.sum(np.exp(output), axis=0)

        show_output = list(output)        
        cMax = max(show_output)

        max_list = []

        # choose best likely output (nearest 1)
        for (index, x) in enumerate(show_output):
            
            if x == cMax and cMax != 0:
                max_list.append(index)

            # clip x if too smallest to prevent reversing value
            if x < 0.1e-10:
                show_output[index] = 0

        # having more that one answer -> random choose one of available 
        if len(max_list) > 1:
            outputx = random.choice(max_list)
        elif len(max_list) == 0:
            outputx = None
        else:
            outputx = max_list[-1]
        
        return outputx


    def updateResultPredict(self, predic_result, real_result):
        """
            collecting Predict result and calculate predicting static and print predict result
        """
        # collect predict result
        if predic_result == real_result:
            self.predict_true += 1
            self.truePositive[predic_result]+=1

        if predic_result != real_result:
            self.predict_false += 1
            self.falsePositive[predic_result]+=1
            self.falseNegative[real_result]+=1
                
        # print result
        print 'predic:', predic_result, '     real_result:', real_result


    def showPredictStat(self):
        # number of action in this round
        sample_number = self.predict_false + self.predict_true

        if sample_number > 0:
            print "-----Overall--------"
            print 'Sample:', sample_number
            print 'True:', self.predict_true
            print 'False:', self.predict_false
            print 'Prediction Percent:', self.predict_true / sample_number * 100

            # save Predictor static
            if os.path.isfile(G('save-path-static-predictor') + self.environment.username + '_predictor.json'):
                f = open(G('save-path-static-predictor') + self.environment.username + '_predictor.json', 'r')
                x = json.load(f)
                f.close()
                f = open(G('save-path-static-predictor') + self.environment.username + '_predictor.json', 'w')
                x['cycle'] += 1
                x['true'] += self.predict_true
                x['false'] += self.predict_false
                x['sample'] += sample_number
                x['percent'] = x['true'] / x['sample'] * 100
                # x['total_train_time']+=self.train_time
                f.write(json.dumps(x))
                f.close()

            else:
                f = open(G('save-path-static-predictor') + self.environment.username + '_predictor.json', 'w')
                x = {}
                x['cycle'] = 1
                x['true'] = self.predict_true
                x['false'] = self.predict_false
                x['sample'] = sample_number
                x['percent'] = x['true'] / x['sample'] * 100
                # x['total_train_time']=self.train_time
                f.write(json.dumps(x))
                f.close()
            
            for word in self.words:
                print "--",word,"--"
                precision=(float(self.truePositive[word])/(self.truePositive[word]+self.falsePositive[word]))*100
                recall=(float(self.truePositive[word])/(self.truePositive[word]+self.falseNegative[word]))*100
                print "Precision(%s/%s):%s"%(self.truePositive[word],(self.truePositive[word]+self.falsePositive[word]),precision)
                print "Recall(%s/%s):%s"%(self.truePositive[word],(self.truePositive[word]+self.falseNegative[word]),recall)
                print "-----------------"

            truePos=0
            flasePos=0
            falseNeg=0

            for word in self.words:
                if word != 'wait':
                    truePos+=self.truePositive[word]
                    falsePos+=self.falsePositive[word]

            falseNeg=self.falsePositive['wait']        
            precision=(float(truePos)/(truePos+falseNeg))*100
            recall=(float(truePos)/(truePos+falseNeg))*100

            print "Precision(%s/%s):%s"%(truePos,(truePos+falsePos),precision)
            print "Recall(%s/%s):%s"%(truePos,(truePos+falseNeg),recall)
            print "-----------------" 
      

    def trainNEAT(self, features, outputs):
        """Training Function for predictor which use NEAT"""

        # task with pca
        task = predictorTask(True, features, outputs)

        # task without pca
        # task = predictorTask(True,self.input_mem,self.output_mem)

        nodecounts = defaultdict(int)

        # if we already have save
        if self.brain is not None:
            pop = self.brain
            for i in xrange(1):
                pop.epoch(generations=G('num-generation'), evaluator=task, solution=task, reset=False)
                nodecounts[len(pop.champions[-1].node_genes)] += 1

        # if we do not have save yet
        else:
            # new genotype function for new brain -> 3 input types -> [input , hidden , output]
            genotype = lambda : NEATGenotype(inputs=len(features[0]), outputs=6, weight_range=(-3., 3.), types=['sigmoid', 'sigmoid', 'sigmoid'])

            pop = NEATPopulation(genotype, popsize=G('num-pop'))
            for i in xrange(1):
                pop.epoch(generations=G('num-generation'), evaluator=task, solution=task, reset=True) 
                nodecounts[len(pop.champions[-1].node_genes)] += 1

        return pop


    def trainNeural(self, features, outputs):

        # translate output
        outputs = np.argmax(outputs, axis=1)

        # train NETWORK with NeuralNetwork
        network = OneVsRestClassifier(LinearSVC(random_state=0))
        network.fit(features, outputs)
       
        print "score:",network.score(features, outputs)
        return network
    

    def trainPredictorAI(self, features, outputs):
        # do not count and train with small game -> something wrong in game
        if len(self.input_mem) <= 50:
            self.environment.restartGame()
            return

        # initial timer to get learning time
        start_time = time.time()

        # Train NETWORK with NEAT
        # network=self.trainNEAT(features,outputs)

        # Train NETWORK with Standard Neural Net
        network = self.trainNeural(features, outputs)

        # Show time to Train
        self.train_time = time.time() - start_time
        print '--- %s seconds ---' % self.train_time

        # Save network
        self.saveToFile(G('save-path-brain'), self.environment.username + '_network.json', 'obj', network)
        
        # Auto restart with fix cycles
        # if G('simulation-auto-restart-after-learn') and G('cycle-simulation') > self.environment.cycle_game:
            # self.environment.restartGame()

   
    def getFeature(self, environment):
        """Generate table from environment -> using for Predictor Train"""

        standard_input = [0] * 5
        environment_input = environment.getFeature()

        if len(environment_input) > 0:
            for (key, value) in environment_input:
                
                # static feature
                if key == 'player_life':
                    standard_input[0] = value
                if key == 'ai_life':
                    standard_input[1] = value
                if key == 'player_power':
                    standard_input[2] = value
                if key == 'ai_power':
                    standard_input[3] = value
                if key == 'flag_status':
                    standard_input[4] = value
                        
            # standard_input.append(environment.board._total_time)
            
            # get everything in board to only one table -> using PCA
            # board_input = environment.getBoardDetail(environment_input)
            # standard_input.extend(board_input)

            # Separate item in board in many table -> Using PCA
            bomb_input = self.environment.getBoardBomb(environment_input)
            redHeard_input = self.environment.getBoardRedHeart(environment_input)
            greenHeart_input = self.environment.getBoardGreenHeart(environment_input)
            flag_input = environment.getBoardFlag(environment_input)
            block_input = self.environment.getBoardBlock(environment_input)
            boom_input = self.environment.getBoardBoom(environment_input)
            player_position = self.environment.getBoardPlayer(environment_input)
            ai_position = self.environment.getBoardAi(environment_input)
            standard_input.extend(bomb_input + redHeard_input + greenHeart_input + block_input + boom_input 
                                + player_position + ai_position + flag_input)

            return standard_input


    def guessPlayerAction(self, board):
        """
            Get Last Action by using the changing of position -> use to get oppenent last action
        """

        (ex, ey) = board.getPosition(self.enemy)
        oex = self.enemy_position['enemy_x']
        oey = self.enemy_position['enemy_y']
        
        guess = (0, 0, 0, 0, 0, 0)

        enemy_bomb = board.getLocationsWithManOfType(bomb.Bomb)

        # left:1 right:2 up:3 down:4 wait:0 bomb:5
        if oex > ex and oey == ey:
            guess = (1, 0, 0, 0, 0, 0)
            # guess = "left"
        if oex < ex and oey == ey:
            guess = (0, 1, 0, 0, 0, 0)
            # guess = "right"
        if oey > ey and oex == ex:
            guess = (0, 0, 1, 0, 0, 0)
            # guess = "up"
        if oey < ey and oex == ex:
            guess = (0, 0, 0, 1, 0, 0)
            # guess = "down"
        if oex == ex and oey == ey:
            guess = (0, 0, 0, 0, 0, 1)
            # guess = "wait"
            for (bx, by) in enemy_bomb:
                if bx == oex and by == oey:
                    guess = (0, 0, 0, 0, 1, 0)
                    # guess= "bomb"
                    break

        self.enemy_position['enemy_x'] = ex
        self.enemy_position['enemy_y'] = ey

        return guess
   

    def saveToFile(self, path, namefile, type_data, data):
        """
            generic save function for all file in program
            type_date -> 'obj' for save as python save obj file
                      -> 'json' to save as json file
        """

        f = open(path + namefile, 'w')

        if type_data == 'obj':
            pickle.dump(data, f)
        if type_data == 'json':
            f.write(json.dumps(data))

        f.close()


    def loadFromFile(self, path, namefile, type_data):
        """Generic load function for all required file in program
           type_date -> 'obj' for load from python save obj file
                      -> 'json' to load from json file 
        """
        if os.path.isfile(path + namefile):
            f = open(path + namefile, 'r')
        
            if type_data == 'obj':
                x = pickle.load(f)
            if type_data == 'json':
                x = json.load(f)
            
            f.close()            
            return x

        else:
            # return None if does not have file yet
            return None


    def saveHistory(self):
        """Saving Function for action history which will be used as input on Predictor Training"""
        
        # No PCA -> Load History log
        history = self.loadFromFile(G('save-path-history'), self.environment.username + '.json', 'json')

        # print history
        if history is not None:

            # Decode unzip zip data
            history = zip(*history)

            # Decode array of array to array of tuple
            history_input = map(tuple, history[0]) + self.input_mem
            history_output = map(tuple, history[1]) + self.output_mem

        else:

            history_input = self.input_mem
            history_output = self.output_mem

        # Add Last History -> last Action on End game
        history = zip(history_input, history_output)

        # Save History log
        self.saveToFile(G('save-path-history'), self.environment.username + '.json', 'json', history)

        return (history_input, history_output)
      

    def onEndangered(self, event):
        """We were endangered"""
        self.log.debug('%s is endangered - escaping' % event.man.getNiceName())
        self.chooseEscapeDestination(event.man, event.board)
        self.state.current = S_ESCAPING
    

    def onArrived(self, event):
        """We arrived at our destination"""

        self.log.debug('%s arrived at destination' % event.man.getNiceName())
        if event.src in (S_CLEARING):
            self.tryToDropBomb(event.man, event.board)
        
        # elif event.src == S_ESCAPING:
        # When we arrive at the escape square then we should wait for quite a bit to allow the bomb to go off
            # self._last_move -= G('ai-wait-cycles') * self.move_interval
        

    def findShortest(self,(mx,my),possible,board,exceptList):
        
        paths=[]
        
        for strategy, x, y in possible:

            pass_it=False
            
            for ex,ey in exceptList:
                if (x,y) == (ex,ey):
                    pass_it=True
                    break
                
            if not pass_it:
                try:
                    path = board.getPath((mx, my), (x, y))
                    paths.append((len(path), path,"restricted"))
                except NoPath:
                    # Ok - there is no path
                    path = board.getPath((mx, my), (x, y),unrestricted=True)
                    #pass
                    paths.append((len(path)+5, path,"unrestricted"))
        
        if paths:
                paths.sort(lambda a, b: cmp(a[0], b[0]))
                return paths[0]
        else:
            return False
    

    def makeNextMove(self, man, board):
        """Make the next move to our destination"""
        
        mx, my = board.getPosition(man)
       
        event = serge.blocks.settings.Bag()
        event.man = man
        event.board = board
        
        if not self.selected_path :
            
            if self.state.current!=S_WAITING:
                self.state.arrived(man=man, board=board)
            else:
                if not board.isSafe(board.getPosition(man)):
                    if self.state.current != S_ESCAPING:
                        self.onEndangered(event)
                    self.state.endangered(man=man, board=board)
                else:
                    self.state.current = S_WAITING
        
        else:
            
            nx, ny = self.selected_path[0]
            
            # Only make the move if we can
            if not board.canOccupy(man, (nx, ny)):

                self.log.debug('%s cannot move to %s, %s' % (man.getNiceName(), nx, ny))
                
                if board.isSafe((nx, ny)):
                    self.state.current = S_WAITING
                    
                    if board.getOverlapping((nx, ny))[0].__class__.__name__ == "Man":
                        self.tryToDropBomb(event.man, event.board)  
                
                else:
                    if self.state != S_ESCAPING:
                        self.onEndangered(event)
                    self.state.endangered(man=man, board=board)
            
            elif board.isSafe((nx, ny)) or (self.state.current == S_ESCAPING and not board.isDeadly((nx, ny))):
                
                # Only if it is safe or if we are escaping
                # if not self.walk.isPlaying():
                    # self.walk.play()
                
                board.moveMan(man, (nx - mx, ny - my))
                self.environment.saveDataTrain(self.environment.getFeature(),(nx - mx, ny - my))
                self.selected_path.pop(0)
            
            else:
                
                # Ok - it isn't safe to make the move
                self.log.debug('Not safe for %s to move to %s, %s' % (man.getNiceName(), nx, ny))
                
                if not board.isSafe((mx, my)):
                    if self.state.current != S_ESCAPING:
                        self.onEndangered(event)
                    self.state.endangered(man=man, board=board)
                
                else:
                    self.state.current = S_WAITING


    def tryToDropBomb(self, man, board):
        """Try to drop a bomb"""

        if self.isSafeToDropBomb(man, board):
            self.makeMove('b', man, board)
            serge.sound.Sounds.play('drop')
            self.state.drop_bomb()
            self.chooseEscapeDestination(man, board)
        
        else:
            event = serge.blocks.settings.Bag()
            event.man = man
            event.board = board
            if not board.isSafe(board.getPosition(man)):
                if self.state.current != S_ESCAPING:
                    self.onEndangered(event)
                self.state.endangered(man=man, board=board)
            else:
                self.state.current = S_WAITING
            self.log.info('Not safe for %s to drop bomb' % man.getNiceName())
    

    def makeMove(self, move, man, board):
        """Make a move"""

        if move == ' ':
            pass
        elif move == 'b':
            board.dropBomb(man)
        else:
            direction = serge.blocks.directions.getVectorFromCardinal(move)
            board.moveMan(man, direction)   
    

    def canDestroyEnemy(self,man,board):
        """Check if We can do something to Destroy enemy"""

        try:
            # player location
            mx, my = board.getPosition(man)
            # ai location
            ex, ey = board.getPosition(self.enemy)
            bomb_distance=man.bomb_distance
        except:
            return False

        try:
            path_distance=len(board.getPath((mx,my),(ex,ey)))
        except NoPath:
            return False        
                      
        if mx==ex and abs(my-ey)<=bomb_distance and len(board.getPath((mx,my),(ex,ey)))==abs(my-ey):
            return True
        elif my==ey and abs(mx-ex)<=bomb_distance and len(board.getPath((mx,my),(ex,ey)))==abs(mx-ex):
            return True

        return False

    
    def isSafeToDropBomb(self, man, board):
        """Check if it is safe to drop a bomb at the current time"""
        
        # It is safe if we can get to a location which is safe beyond the blast radius of our bomb
        x, y = board.getPosition(man)
        blast_radius = man.bomb_distance
        max_search = 2 * blast_radius

        for _, (sx, sy) in board.breadthFirstDestinationSearch(man, (x, y)):

            if (sx != x and sy != y) and board.isSafe((sx, sy)) and board.pathIsSafe(board.getPath((x,y),(sx,sy))):
                # Ok, out of line of fire of this bomb and safe
                return True
            
            elif (abs(sx - x) > blast_radius or abs(sy - y) > blast_radius) and board.isSafe((sx, sy)) and board.pathIsSafe(board.getPath((x,y),(sx,sy))):
                # Ok, outside of the blast radius
                return True
            
            elif abs(sx - x) + abs(sy - y) > max_search:
                # Nothing close to us
                return False
        else:
            # No path to safety
            return False

        
    def makeAction(self, move):
        """Make a move"""

        mx, my = self.environment.board.getPosition(self.environment.ai)
        direction = None
        
        if move == 'bomb':
            self.environment.board.dropBomb(self.environment.ai)
        if move == 'wait':
            return False
        if move == 'left':
            direction = (-1, 0)
        elif move == 'right':
            direction = (+1, 0)
        elif move == 'up':
            direction = (0, -1)
        elif move == 'down':
            direction = (0, +1)

        if direction is not None :   
            if self.environment.board.canMove(self.environment.ai,direction):
                self.environment.board.moveMan(self.environment.ai, direction)
            else:
                return False
            
        return True
    
    def chooseDestination(self, man, board,enviroment,action):
        """Choose the next destination"""

        self.log.debug('Looking for possible attack locations')
        
        # Pick attack locations
        
        # player location
        mx, my = board.getPosition(man)
        # ai location
        ex, ey = board.getPosition(self.enemy)
        
        if action == "escape":
            self.chooseEscapeDestination(man,board)
            return True
        else :
            
            possible = []
            if action == "red_heart" or action == "shortest" or action =="worth":
                possible.extend([("red_heart", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.RedHeart)])
            if action =="green_heart" or action == "shortest" or action =="worth":
                temp=[]
                temp.extend([("red_heart", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.RedHeart)])
                possible.extend([("green_heart",nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.Heart) if (("red_heart",(nx,ny))) not in temp ])
            if action == "flag" or action == "shortest" or action =="worth":
                possible.extend([("flag", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.Flag)])
            if action == "clearing" :
                possible.extend([("block",x, y) for x in range(19) for y in range(19) if self.board.canDestroy((x, y)) ] )
            if action =="aggressive":
                if self.canDestroyEnemy(man,board):
                    self.tryToDropBomb(man,board)
                    return True
                else :
                    possible.append(("enemy",ex,ey))
            
            # Find the closest one we can get to
            paths = []
            
            for strategy, x, y in possible:

                if (mx, my) == (x, y):
                    continue
                
                try:
                    path = board.getPath((mx, my), (x, y))
                    paths.append((len(path), path,"restricted"))
                except NoPath:
                    # Ok - there is no path
                    path = board.getPath((mx, my), (x, y),unrestricted=True)
                    #Pass
                    paths.append((len(path)+5, path,"unrestricted"))
            
            distant=[]
            
            # Find shortest, if there is one
            if paths:

                paths.sort(lambda a, b: cmp(a[0], b[0]))
                if action == "worth":
                    if len(paths)>=3:
                        for i in range(0,3):
                            try:
                                distant.append(paths[i])
                                next_path=paths[i]
                                exceptList=[]
                                exceptList.append(next_path[1][-1])
                                
                                for j in range(0,4):
                                    
                                    next_path=self.findShortest(next_path[1][-1],possible,board,exceptList)
                                    if not next_path:
                                        break

                                    exceptList.append(next_path[1][-1])
                                    
                                    if next_path[0]<10:
                                        distant[i] = list(distant[i])
                                        distant[i][0]=(distant[i][0]*(j+1)+next_path[0])/(j+2)
                                        distant[i] = tuple(distant[i])
                                    else:
                                        break
                            except :
                                break
                        distant.sort(lambda a, b: cmp(a[0], b[0]))
                        paths=distant

                if paths[0][2] == "restricted":
                    self.selected_path = paths[0][1][1:]
                    self.state.current = S_MOVING
                
                else:
                    # No paths so find a clearing path
                    self.chooseClearingDestination(man, board,paths[0][1])
                
                return True
            
            else:
                self.state.current = S_WAITING
            
            return True

    
    def chooseEscapeDestination(self, man, board):
        """Choose a location to escape to"""
        
        # From the current square - do a breadth first
        # search of the places that we can get to. Choose the path that is safe
        x, y = board.getPosition(man)

        for _, (dx, dy) in board.breadthFirstDestinationSearch(man, (x, y)):
            if board.isSafe((dx, dy)):
                self.log.debug('Found escape square %s, %s for %s' % (dx, dy, man.getNiceName()))
                break
        else:
            # No place to go - just pick the last anyway
            self.log.debug('No escape square for %s' % man.getNiceName())
            self.selected_path = []
            return
        
        path = board.getPath((x, y), (dx, dy))
        self.selected_path = path[1:]

    
    def chooseClearingDestination(self, man, board,path):
        """Choose a destination to drop a bomb to clear a way"""
        self.log.debug('Looking for clearing path for %s' % man.getNiceName())
        
        # Find the best path from us to the enemy
        # x, y = board.getPosition(man)
        # ex, ey = board.getPosition(self.enemy)
        # try:
        #     path = board.getPath((x, y), (ex, ey), unrestricted=True)
        # except NoPath:
        #     path = None
        
        # Make sure we have a path
        if not path:
            self.log.debug('No path found')
            return
        
        # Look for the bombing location
        chosen_path = []
        
        for (px, py) in path[1:]:
            if board.isDestructible((px, py)):
                self.log.debug('Found path')
                break
            chosen_path.append((px, py))
            
        else:
            # This probably means that a block was already destroyed
            # so we should just wait for another cycle
            self.log.debug('No path with destructible found')
            return
        
        self.state.current = S_CLEARING
        self.selected_path = chosen_path
    

    def getState(self):
        """Generate State for RL in whole board type -> too many state -> too slow for learning"""
        return ''.join(str(x) for x in self.environment.getBoardDetail(self.environment.getFeature()))
    

    def getState(self,environment,board):
        """Generate State Number for RL in seperated importent information way"""
        
        #How many heart you have
        player_hearts=environment.player_hearts.getValue('hearts')
        
        #How manu heart enemy have
        ai_hearts=environment.ai_hearts.getValue('hearts')

        #how many flag
        possible=[]
        possible.extend([("flag", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.Flag)])
        num_flag=len(possible)

        #How many red_heart
        possible=[]
        possible.extend([("red_heart", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.RedHeart)])
        num_red_heart=len(possible)
        
        #HOW many green_heart
        temp=[]
        possible=[]
        temp.extend([("red_heart", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.RedHeart)])
        possible.extend([("green_heart",nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.Heart) if (("red_heart",(nx,ny))) not in temp ])
        num_green_heart=len(possible)
        
        # status player heart is high medium or low   
        if player_hearts>5:
            player_hearts_status=5
        else :
            player_hearts_status=player_hearts

        # status ai heart is high medium or low   
        if ai_hearts>5:
            ai_hearts_status=5
        else :
            ai_hearts_status=ai_hearts

        # flag status -> ai,player or unoccupy
        if self.environment.flag_status_panel.flag_position==0:
            flag_status=2
        elif self.environment.flag_status_panel.flag_position>0:
            flag_status=3
        elif self.environment.flag_status_panel.flag_position<0:
            flag_status=1
        
        #Near Player
        try:
            mx, my = board.getPosition(environment.player)
            ex, ey = board.getPosition(environment.ai)
        except:
            near_enemy=0
        else:
            try:
                aiToPlayerPath=board.getPath((mx,my),(ex,ey))
            except NoPath:
                aiToPlayerPath=[]
            if len(aiToPlayerPath)<6:
                near_enemy=1
            else:
                near_enemy=0

        #Has red_heart in map?
        has_red_heart=1 if num_red_heart else 0

        #Has green_heart in map?
        has_green_heart=1 if num_green_heart else 0
        #Has flag
        has_flag=1 if num_flag else 0

        #Can kill buy red heart
        can_kill_by_red_heart=1 if num_red_heart-player_hearts>=0 else 0
        #Can die buy red heart
        can_die_by_red_heart=1 if num_red_heart-ai_hearts>=0 else 0
        
        #collect available_path value
        px, py = self.board.getPosition(self.enemy)
        ax, ay = self.board.getPosition(self.man)
        player_path = self.board.getArea(self.enemy,self.board)
        ai_path = self.board.getArea(self.man,self.board)

        #player_path level
        if player_path>20:
            player_path_status=5
        elif player_path>=15 and player_path<=20:
            player_path_status=4
        elif player_path>=10 and player_path<15:
            player_path_status=3
        elif player_path>=5 and player_path<10:
            player_path_status=2
        else :
            player_path_status=1

        #ai_path level
        if ai_path>20:
            ai_path_status=5
        elif ai_path>=15 and ai_path<=20:
            ai_path_status=4
        elif ai_path>=10 and ai_path<15:
            ai_path_status=3
        elif ai_path>=5 and ai_path<10:
            ai_path_status=2
        else :
            ai_path_status=1

        # collect power value 
        # player_power = self.environment.PLAYER_Explosion_Distance
        # ai_power = self.environment.AI_Explosion_Distance

        #player power level
        # if player_power>4:
        #     player_power_status=3
        # elif player_power>=3 and player_power<=4:
        #     player_power_status=2
        # else:
        #     player_power_status=1

        #ai power level
        # if ai_power>4:
        #     ai_power_status=3
        # elif ai_power>=3 and ai_power<=4:
        #     ai_power_status=2
        # else:
        #     ai_power_status=1

        #Collect State Number   
        return ''.join(str(x) for x in [player_hearts_status,ai_hearts_status,has_red_heart,has_green_heart,has_flag,
            can_kill_by_red_heart,can_die_by_red_heart,flag_status,player_path_status,ai_path_status,near_enemy])
    

    def getPlayRound(self):
        """
            Calculate number of play round -> only count if history have 'train' labeled 
            this number is used for calculating e-epsilon
        """

        playRound = 0

        # load player Static from file
        x = self.loadFromFile(G('save-path-static'),self.environment.username+'.json',"json")
        
        if x is not None:
            for y in x:
                if y[unicode('flag')]=="train":
                    playRound = playRound + y[unicode('win')] + y[unicode('lose')]
        else:
            playRound = 0

        return playRound

                                
    def getActionReward(self,state,action,term,debug=0):
        """
            Calculate State Reward from State Number (using seperate importent information way)
                [player_hearts_status, 
                ai_hearts_status,
                has_red_heart,
                has_green_heart
                has_flag,
                can_kill_by_red_heart,
                can_die_by_red_heart,
                flag_status,
                player_path_status,
                ai_path_status,
                player_power_status,
                ai_power_status]

            term is useing for debug -> avai_path,heart,flag_status,power
        """
        
        reward = 0

        # define factor
        avai_path_factor = 0.3
        heart_factor = 0.3
        flag_factor = 1.5
        #power_factor = 0.1
        
        # break down state number to list 
        state_list = [int(i) for i in str(state)]
        
        # avai_path -> dif between available path
        if 'available_path' in term:
            player_avai_path = state_list[8]
            ai_avai_path = state_list[9]
            dif_avai_path = ai_avai_path - player_avai_path
            reward = reward + (avai_path_factor * dif_avai_path)

        # heart -> dif between hp
        if 'heart' in term:
            p_heart =  state_list[0]
            a_heart = state_list[1]
            reward = reward + (heart_factor * (5-p_heart)) + (heart_factor * (a_heart - 5 ))

        # flag -> flag status
        if 'flag_status' in term:
            flag_status = state_list[7] - 2
            reward = reward + (flag_factor * flag_status )
            
        # power -> no used
        # if 'power' in term:
        #     p_power = state_list[10]
        #     a_power = state_list[11]
        #     dif_power = a_power - p_power
        #     reward = reward + (power_factor * dif_power)

        #debug part -> level 1 2 0
        if debug==2:
            print 'State number',state
            
            if 'avai_path' in term:
                print "Dif Available Path :",dif_avai_path
            if 'heart' in term:
                print 'Player Heart :',p_heart
                print 'AI Heart :',a_heart
            if 'flag_status' in term:
                print 'Flag Status :',flag_status
            # if 'power' in term:
            #     print 'Dif Power :',dif_power
            print "State' Reward :",reward
            print '------------------------'   
        
        if debug==1:
            print 'State Reward :',reward
        
        return reward       
    

    def reinforcementMdpDecisionMaking(self,state,typeAction,typeSave,allAction):
        """
            Decision Making Function Using MDP Value Iteration method
            available Strategy -> ["red_heart","green_heart","flag","clearing","shortest","worth","aggressive"]
            typeAction -> 'move' make Decision on each action
                       -> 'stategy' (so many using word 'strategy' so we use word 'stategy' instead )
            typeSave -> 'overall' only have reward on end game state
                     -> 'xxxxxx' give reward on every state changing 
        """
        
        action=None
        has_change=True
        
        if typeAction == "move":

            if hasattr(self.stategy,state):
                action=self.stategy[state] 
            else :
                action=allAction[random.randint(0, len(allAction)-1)]
            result=self.makeAction(action)
        
        elif typeAction == "stategy":
            
            # if state does not change -> dont change action
            if self.recently_state==state:
                action=self.recently_stategy
                has_change=False
            else:
                # if we have policy -> choose from policy
                if unicode(state) in self.stategy:
                    # calculate e-epsilon greedy with play round number
                    if G('MDP-Learning-On'):
                        if G('explorer-point')=="auto":
                            deno = math.floor(self.getPlayRound()/50)+1
                            epsilon = 0.8/deno
                        else:
                            epsilon=G("explorer-point")
                    else:
                        epsilon=0

                    # choose best action
                    if random.uniform(0,1)>epsilon:
                        action=self.stategy[state]
                    # choose random for explore
                    else:
                        action=allAction[random.randint(0, len(allAction)-1)]

                # if does not have policy yet -> random choose
                else:
                    action=allAction[random.randint(0, len(allAction)-1)]
                
                print 'state:',state
                print 'strategy:',action

            self.recently_state=state
            self.recently_stategy=action

            result=self.chooseDestination(self.man,self.board,self.environment,action)   

            # get reward
            determine_term = ['available_path','heart','flag_status']
            reward = self.getActionReward(state,action,determine_term,debug=0)
        
        if not has_change:
            return True
        
        # save history for learning on end game
        if typeSave == "overall":
            self.myActionHistory.append([state,action])
        else :
            # save for end game
            if self.environment._game_over:
                if self.environment.playerWin:
                    self.myActionHistory.append([state,action,-5])
                else:
                    self.myActionHistory.append([state,action,5])
            # save for the others state step
            else :
                if result:
                    self.myActionHistory.append([state,action,reward])
                else:
                    self.myActionHistory.append([state,action,reward])

        if result:
            return True  
        else:
            return False


    def reinforceMdpTraining(self,actionHistory,rewards):
        """RL Training Function for MDP Value Iteration"""
        
        """
        Nathan'RL state pattern 
            obs1 = [[u"A",u"F"],[u"A",u"L"],[u"Prize","uF"]]
            obs2 = [[u"C",u"R"],[u"D",u"F"],[u"B",u"B"],[u"D",u"L"]]
            obs3 = [[u"C",u"F"],[u"A",u"R"],[u"B",u"L"],[u"A",u"L"],[u"Prize",u"L"]]
            actionHistory = [obs1,obs2,obs3]
            rewards = [0,0,1]
        """
        
        # gamma weight
        gamma=0.9

        # set time to calculate learning time
        start_time=time.time()

        # call learning method for library -> 'overall' give reward only at last step
        if G("save-MDP-reward-type")=="overall":
            model = l.learn(actionHistory,gamma,rewards)
        else:
            model = l.learn(actionHistory,gamma)
        
        # save learned policy to file
        self.saveToFile(G('save-path-model'),self.environment.username+".json","json",model)
        
        # show learning time
        print "<>",time.time()-start_time

    
    def TDDecisionMaking(self,state,typeAction,allAction):
        """
            Decision Making Function for TD(q-learning,Sarsa)
            available Strategy -> ["red_heart","green_heart","flag","clearing","shortest","worth","aggressive"]
            typeAction -> 'move' make Decision on each action
                       -> 'stategy' (so many using word 'strategy' so we use word 'stategy' instead )
            typeSave -> 'overall' only have reward on end game state
                     -> 'xxxxxx' give reward on every state changing 
        """
        
        action=None
        has_change=True
        playRound = self.getPlayRound()

        if typeAction == "move":
        
            if hasattr(self.stategy,state):
                action=self.stategy[state] 
            else :
                action=allAction[random.randint(0, len(allAction)-1)]

            result=self.makeAction(action)
        
        elif typeAction == "stategy":

            if self.recently_state==state:
                action=self.recently_stategy
                has_change=False
            
            else:
                # if we have known this state already then use normal choosing strategy method
                if state in self.TDStateDict:
                    action = self.TDChoosingActionPolicy(state,playRound)

                # if we have not known this state then initial it in state_dict
                else:
                    self.TDStateDict[state] = [0] * len(self.actions)

                    # add bias to unappropriated strategy for this state
                    actionFilter = self.filterAction(state,self.actions)
                    
                    for index,act in enumerate(self.actions):
                        if actionFilter[index] == 'fail':
                            self.TDStateDict[state][index]= -3

                    action = self.TDChoosingActionPolicy(state,playRound)

                # collect pair of action and state for learning on end game
                self.collectingTDData(state,action)

                # print out state and action we choose
                print "state:",state
                print "stategy:",action

            self.recently_state=state
            self.recently_stategy=action
            
            result=self.chooseDestination(self.man,self.board,self.environment,action)   

        if not has_change:
            return True
        
        if result:
            return True  
        else:
            return False


    def TDChoosingActionPolicy(self,state,playRound):
        """Real Choosing Action Function with e-epsilon greedy (Only for TD)"""

        if G('TD-Learning-On'):
            if G('explorer-point')=="auto":
                deno = math.floor(self.getPlayRound()/50)+1
                epsilon = 0.8/deno
            else:
                epsilon=G("explorer-point")
        else:
            epsilon=0

        # random choose
        if random.random()<epsilon:
            choosedAction = random.choice(self.actions)
        
        # choose the best
        else:
            posActions = []
            maxAction = -100000
            
            # find max value Action
            for index,act in enumerate(self.TDStateDict[state]):
                if act > maxAction:
                    maxAction = act

            # collect posible action (which is maxaction) 
            for index,act in enumerate(self.actions):
                if self.TDStateDict[state][index] == maxAction:
                    posActions.append(act)
            
            choosedAction = random.choice(posActions)

        return choosedAction


    def collectingTDData(self,state,action):
        
        actionNum = 0

        # find action number according to available action and append for training
        for index,act in enumerate(self.actions):
            if act == action:
                self.TDTrainingData.append((state,index))


    def TDTraining(self,learningType):
        """
            Training function for TD RL
            learningType -> 'q_learning' / 'sarsa'
        """

        lastState = None
        lastAction = None
        nextState = None
        nextAction = None
        turn = 0
        start_time=time.time()

        for state,actionNum in self.TDTrainingData:
            
            turn = turn + 1

            if lastState == None:
                lastState = state
                lastAction = actionNum
                continue
            
            nextState = state
            nextAction = actionNum

            if nextState == 'win':
                reward =5
                print 'AI-Win'
            elif nextState == 'lose':
                reward = -5
                print 'AI-Lose'
            else:
                determine_term = ['available_path','heart','flag_status']
                reward = self.getActionReward(nextState,nextAction,determine_term)

            print 'turn :',turn
            print 'update state',lastState
            print 'update action',self.actions[lastAction]
            print 'update dict',self.TDStateDict[lastState]
            print 'reward :',reward
            
            if learningType == 'q_learning':
                self.updateQFunction(lastState,nextState,lastAction,reward)
            elif learningType == 'sarsa':
                self.updateSarsaFunction(lastState,nextState,lastAction,nextAction,reward)
            
            print 'state dict :',self.TDStateDict[lastState]

            lastState = nextState
            lastAction = nextAction
        
        print "-------------",time.time()-start_time,"--second----"


    def TDSetGameOver(self):
        """Collect Data only on End Game state to give reward for end game"""
        
        if self.environment.playerWin:
            self.TDStateDict['lose'] = [0] * len(self.actions)
            self.collectingTDData('lose',self.actions[-1])
        else:
            self.TDStateDict['win'] = [0] * len(self.actions)
            self.collectingTDData('win',self.actions[-1])

        print 'state number :', len(self.TDStateDict)
        print 'samples :', len(self.TDTrainingData)
        
        self.TDTraining(G('TD-Learning-Method'))


    def updateQFunction(self,lastState,nextState,lastAction,reward):
        """Policy update function for Q-Learning"""
        # define factor
        alpha = 0.2
        discount = 0.9

        
        # q-function
        self.TDStateDict[lastState][lastAction] = (self.TDStateDict[lastState][lastAction] + 
                (alpha * (reward + (discount * max(self.TDStateDict[nextState])) - self.TDStateDict[lastState][lastAction])))


    def updateSarsaFunction(self,lastState,nextState,lastAction,nextAction,reward):

        # define factor
        alpha= 0.2
        discount = 0.9

        # mean calculation for simulation state -> using only for sarsa (if online)
        # mean_next=reduce(lambda x, y: x + y, self.TDStateDict[nextState]) / len(self.TDStateDict[nextState])
        # epsilon=G('explorer-point')

        # sarsa-function
        self.TDStateDict[lastState][lastAction] = (self.TDStateDict[lastState][lastAction] +
                (alpha * (reward + (discount * self.TDStateDict[nextState][nextAction]) - self.TDStateDict[lastState][lastAction])))

        # self.TDStateDict[lastState][lastAction] = (self.TDStateDict[lastState][lastAction] +
        # (alpha * (reward + (discount * (epsilon*(mean_next) + (epsilon-1)*(max(self.TDStateDict[nextState])))) - self.TDStateDict[lastState][lastAction])))


    def filterAction(self,state,actions):
        """Filter unappropriated strategy in this state to add bias"""

        stateList  = [int(i) for i in str(state)]
        posActions = []

        for act in actions:
            if act == 'red_heart' and stateList[2] == 0:
                posActions.append('fail')
                continue
            if act == 'green_heart' and stateList[3] == 0:
                posActions.append('fail')
                continue
            if act == 'flag' and stateList[4] == 0:
                posActions.append('fail')
                continue
            posActions.append(act)

        return posActions

    
    def updateController(self, interval, man, board, environment):
        """Update the control of the actor"""
        
        self.man=man
        self.environment = environment
        self.board=board
        
        
        self._last_move += interval / 1000.0
        self._total_time = board._total_time
        
        # GAME OVER
        if environment._game_over:

            # reset / remove item on board if auto restart has been set
            if G('simulation-auto-restart'):
                self.environment.startGift = False
                self.environment.gift_box.stop()
                self.environment.gift_box.reset()
            
            # set setupAi to false so its will not load next in round 
            self.setupAi=False

            # restart game if game is too fast so don't count on it -> fix game stop bug
            if self.board._total_time<=7 or len(self.input_mem)==0:
                print "Restart the game"
                self.input_mem = []
                self.output_mem = []
                self.myActionHistory=[]
                environment.restartGame()
                return

            # show game round time
            print self.board._total_time

            # pop last input -> its dead
            self.input_mem.pop()
            
            # increase number of round
            self.environment.cycle_game+=1

            # show Predict Stat
            # self.showPredictStat()
            # print '--- %s samples input---' % len(self.input_mem)
            # print '--- %s samples output---' % len(self.output_mem)

            # Save history -> for predictor history
            if G('save-predictor-history-on'):
                (all_inputs, all_outputs) = self.saveHistory()
                print 'Predictor History : %s samples ---' % len(all_inputs)
            
            # load all collected action History/Reward from file -> MDP History     
            actionsHistory=self.loadFromFile(G('save-path-action'),self.environment.username+"_action.json","json")
            rewards=self.loadFromFile(G('save-path-action'),self.environment.username+"_rewards.json","json")
                
            if actionsHistory is None:
                actionsHistory = []
                rewards=[]
            else:
                print 'Load MDP History Succeed from',len(actionsHistory),'rounds'
                
            # for last action
            if G('save-MDP-reward-type')!="overall": 
                self.reinforcementMdpDecisionMaking(self.getState(environment,board),"stategy","each",self.actions)
            
            # append this round action to actionsHistory
            actionsHistory.append(self.myActionHistory)
            
            if G('save-MDP-action-on'):
                self.saveToFile(G('save-path-action'),self.environment.username +"_action.json","json",actionsHistory)
                
                # give reward at end game
                if self.environment.playerWin:
                    rewards.append(-1)
                    self.saveToFile(G('save-path-action'),self.environment.username +"_rewards.json","json",rewards)
                else:
                    rewards.append(10)
                    self.saveToFile(G('save-path-action'),self.environment.username +"_rewards.json","json",rewards)

            # TD Learning
            if G('TD-Learning-On'):
                self.TDSetGameOver()
                self.saveToFile(G('save-path-TD'),self.environment.username,'obj',self.TDStateDict)


            # PCA
            # X = np.array(all_inputs)
            # if self.pca is None:
            #     pca = PCA(n_components=G('num-feature'))
            #     pca.fit(X)
            # else :
            #     pca = self.pca
            #     # fit new pca ONly use when alway train from scratch
            #     pca.fit(X)

            # all_inputs= pca.transform(X)

            # Save PCA FIT
            # self.saveToFile(G('save-path-pca'),self.environment.username+'_pca.json','obj',pca)
            # print "pca variance",sum(pca.explained_variance_ratio_)

            
                 
            # Run train predictor
            if G('predictor-training-on'):                                                                        
                self.trainPredictorAI(all_inputs, all_outputs)
                print 'Round : %s samples ---' % len(self.input_mem)
                print 'All Time : %s samples ---' % len(all_inputs)

            # MDP Training    
            if G('MDP-Learning-On'):
                self.reinforceMdpTraining(actionsHistory,rewards)
            
            # Everything in shape then Reset value and Restart the game
            self.input_mem = []
            self.output_mem = []
            self.myActionHistory=[]
            if G('simulation-auto-restart') and (G('cycle-simulation')>self.environment.cycle_game):
                self.environment.restartGame()
        
        # RUNNING
        else:

            # bug fixing on auto restart
            if G('simulation-auto-restart'):
                if not self.environment.startGift:
                    self.environment.gift_box.restart(board,environment.world)
                    self.environment.startGift=True


            if not self.setupAi:
            # check if have old pca then load it
                # if os.path.isfile(G('save-path-pca')+self.environment.username+"_pca.json") and self.pca is None :
                #     f = open(G('save-path-pca')+self.environment.username+'_pca.json', 'r')
                #     self.pca = pickle.load(f)
                #     f.close()

                # check if have old network then load it
                self.brain=self.loadFromFile(G('save-path-brain'),self.environment.username+'_network.json',"obj")
                # check if we have policy
                self.stategy=self.loadFromFile(G('save-path-model'),self.environment.username+".json","json")
                self.setupAi=True

            # load TD if TD-On and no stateDict yet
            if not self.TDStateDict and G('ai-decision')=="td":

                TDLoad = self.loadFromFile(G('save-path-TD'),self.environment.username,'obj')
                if TDLoad is not None:
                    self.TDStateDict = TDLoad
                    print 'Load File Complete with',len(self.TDStateDict),'pairs'
            
            # TIME TO MAKE SOME MOVE 
            if self._last_move > self.move_interval :

                if self.stategy is None:
                    self.stategy={}
                
                if self.state.current != S_ESCAPING:
                    if G('ai-decision')=="mdp":
                        if self.reinforcementMdpDecisionMaking(self.getState(environment,board),"stategy",G("save-MDP-reward-type"),self.actions) :
                            self.makeNextMove(man, board)
                    elif G('ai-decision')=="td":
                        if self.TDDecisionMaking(self.getState(environment,board),"stategy",self.actions) :
                            self.makeNextMove(man, board)
                else :
                    self.makeNextMove(man, board)
                
                # Store Last Action -> for predictor
                last_action = self.guessPlayerAction(board)

                # Show Predict result if has a predictor
                if self.brain is not None and len(self.input_mem) > 0:

                    # with pca
                    # self.predict(self.pca.transform(np.array(self.input_mem[-1]).reshape(1,-1))[0],last_action)
                    # without pca
                    self.predict(self.input_mem[-1], last_action)

                # ADD Output
                if len(self.output_mem) != len(self.input_mem) or len(self.input_mem) != 0:
                    self.output_mem.append(last_action)

                # ADD input
                self.input_mem.append(tuple(self.getFeature(environment)))
                
                # reset time
                self._last_move = 0.0






class AIUI(serge.actor.Actor):

    """A user interface to report on the state of the AI"""

    def __init__(self, controller, board, name):
        """Initialise the ui"""

        super(AIUI, self).__init__('debug', name)

        #
        self.controller = controller
        self.board = board

    def addedToWorld(self, world):
        """Added to the world"""

        super(AIUI, self).addedToWorld(world)

        
        # The debugging text
        self.text = serge.blocks.utils.addTextToWorld(world, 'AIUI', self.name, theme, 'ui')
        self.text.tag = 'debug'

        # The destination square
        self.destination = serge.blocks.utils.addVisualActorToWorld(world, 'debug', 'desintation',
                serge.blocks.visualblocks.Rectangle(G('board-cell-size'), G('%s-destination-colour' % self.name)), 'ui')
        self.destination.visible = False
        self.destination.active = G('ai-show-destinations')
    
    def updateActor(self, interval, world):
        """Update the actor"""

        super(AIUI, self).updateActor(interval, world)

        # Update debug text
        self.text.visual.setText('''Strategy: %s
State: %s
Dest: %s
Length: %s'''
                                 % (self.controller.current_strategy,
                                 self.controller.state.current, ('n/a'
                                  if not self.controller.selected_path else self.controller.selected_path[-1]),
                                 ('n/a'
                                  if not self.controller.selected_path else len(self.controller.selected_path))))

        # Update destination square
        if self.controller.selected_path:
            self.destination.visible = True
            self.destination.moveTo(*self.board.screenLocation(self.controller.selected_path[-1]))


