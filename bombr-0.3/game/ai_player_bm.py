

"""Controller for the AI"""

import random
from functools import partial
from collections import defaultdict

import numpy as np

import serge.actor
import serge.common
import thread
import serge.sound
import serge.blocks.directions
import serge.blocks.fysom
import serge.blocks.utils
import serge.blocks.visualblocks
import serge.blocks.settings
from theme import G, theme

from peas.methods.neat import NEATPopulation, NEATGenotype


# from peas.methods.neatpythonwrapper import NEATPythonPopulation

from peas.tasks.xor import XORTask
from peas.networks.rnn import NeuralNetwork

from board import NoPath
import bomb
import powerups
import logging

import time
import pickle

import dill
import os
import json

from sklearn.decomposition import PCA
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC

import reinforce as l

# States

S_WAITING = 'waiting'
S_MOVING = 'moving'
S_ESCAPING = 'escaping'
S_CLEARING = 'clearing'
S_MOVING_FOR_KILL = 'moving-for-kill'

# Events

E_ARRIVED = 'arrived'
E_CHOSE_ATTACK = 'chose_attack'
E_CHOSE_ESCAPE = 'chose_escape'
E_CHOSE_CLEAR = 'chose_clear'
E_DROP_BOMB = 'drop_bomb'
E_ENDANGERED = 'endangered'
E_KILL_SPOTTED = 'kill_spotted'

# Strategies

X_CLOSE_BY = 'close-by'
X_FAR_FROM = 'far-from'
X_PLAYER = 'player'
X_HEART = 'heart'
X_FLAG = 'flag'


class predictorTask(object):

    # Default task input/output pairs
    # INPUTS  = [(0,0), (0,1), (1,0), (1,1)]
    # OUTPUTS = [(-1,), (1,), (1,), (-1,)]

    EPSILON = 1e-100
    inputs = []
    outputs = []

    def __init__(
        self,
        do_all=True,
        match_input=None,
        match_output=None,
        ):

        # do_all = false -> random choose pairs to evaluate -> not working
        self.do_all = do_all
        self.inputs = np.array(match_input, dtype=float)
        self.outputs = np.array(match_output, dtype=float)

    def evaluate(self, network, verbose=False):

        # if network is not neuralNetwork change it to Neural
        if not isinstance(network, NeuralNetwork):
            network = NeuralNetwork(network)

        # feedForward network
        network.make_feedforward()

        # if not network.node_types[-1](-1000) < -0.95:
        #     raise Exception("Network should be able to output value of -1, e.g. using a tanh node.")

        # zip output and input together -> dont need this if we do reinforcement
        pairs = zip(self.inputs, self.outputs)
        random.shuffle(pairs)

        if not self.do_all:
            pairs = [random.choice(pairs)]

        rmse = 0.0

        # run evaluating test on every input/output pairs
        for (i, target) in pairs:

            # Feed with bias
            output = network.feed(i)

            # Grab the output
            output = output[-len(target):]

            # clip output with softmax so we got propability
            outputA = np.exp(output) / np.sum(np.exp(output), axis=0)

            # entropy for softmax
            ans = np.log(outputA)
            err = -np.sum(target * ans)

            # entropy for sigmoid function which i clipped it too -> not done good -> too much fitness
            # err = -np.sum((target*np.log(output+1e-30)) + ((1-target)*np.log(outputA)))

            # print result if verbose is setted
            if verbose:
                print '%r -> %r (%.2f)' % (i, output, err)
           
            rmse += err

        score = 1 / (1 + rmse / len(pairs))
        return {'fitness': score}

    def solve(self, network):
        return int(self.evaluate(network) > 0.6)


class AI(serge.common.Loggable):

    """Represents the AI's control of the actor"""

    long_term_memory = {}

    def __init__(self, enemy=None):
        """Initialise the AI"""

        self.addLogger()
        self.enemy = enemy
        self._last_move = 0.0
        self._total_time = 0.0
        self.move_interval = G('ai-move-interval')
        self.selected_path = None
        self.recently_stategy=None
        self.recently_state=None

       # self.state = serge.blocks.fysom.Fysom({'initial': S_WAITING,
       #         'events': [{'name': E_ARRIVED, 'src': [S_MOVING,
       #         S_CLEARING, S_ESCAPING], 'dst': S_WAITING},
       #         {'name': E_DROP_BOMB, 'src': [S_MOVING, S_CLEARING,
       #         S_ESCAPING, S_WAITING], 'dst': S_ESCAPING},
       #         {'name': E_ENDANGERED, 'src': [S_MOVING, S_CLEARING,
       #         S_WAITING, S_ESCAPING], 'dst': S_ESCAPING},
       #         {'name': E_KILL_SPOTTED, 'src': [S_WAITING],
       #         'dst': S_MOVING_FOR_KILL}]})

       #      'callbacks': {
       #          'onarrived': self.onArrived,
       #          'onendangered': self.onEndangered,
       #      }

        self.state = serge.blocks.fysom.Fysom({
            'initial': S_WAITING,
            'events': [
                {'name': E_ARRIVED,
                 'src': [S_MOVING, S_CLEARING, S_ESCAPING],
                 'dst': S_WAITING,
                 },
                {'name': E_DROP_BOMB,
                 'src': [S_MOVING, S_CLEARING, S_ESCAPING, S_WAITING],
                 'dst': S_ESCAPING,
                 },
                {'name': E_ENDANGERED,
                 'src': [S_MOVING, S_CLEARING, S_WAITING, S_ESCAPING],
                 'dst': S_ESCAPING,
                 },
                {'name': E_KILL_SPOTTED,
                 'src': [S_WAITING],
                 'dst': S_MOVING_FOR_KILL,
                 },
            ],
            'callbacks': {
                'onarrived': self.onArrived,
                'onendangered': self.onEndangered,
            }
        })

        self.walk = serge.sound.Sounds.getItem('walk')
        self.enemy_position = {'enemy_x': 3, 'enemy_y': 3}
        self.heart_grab_distance = G('heart-grab-distance')

        # self.strategy_offsets = {
        #     X_CLOSE_BY: [G('ai-squares-view') * serge.blocks.directions.getVectorFromCardinal(i)
        #                  for i in 'nesw'],
        #     X_FAR_FROM: [3 * G('ai-squares-view') * serge.blocks.directions.getVectorFromCardinal(i)
        #                  for i in 'nesw'],
        # }
        # self.selectStrategy()

        # input_mem and output_mem are used to stagnate information before feed it to network
        self.input_mem = []
        self.output_mem = []

        # these 2 are used in predict function to find the true result of prediction
        self.predict_true = 0.0
        self.predict_false = 0.0

        # pca and brain are what we save in our pickle -> load everytime when the game start to play
        self.pca = None
        self.brain = None
        self.train_time = 0

        # word of action we have
        self.words = ['left', 'right', 'up', 'down', 'bomb', 'wait', ]
        
        # precise and recall evaluation
        self.truePositive = {key: value for (key, value) in zip(self.words,[0,0,0,0,0,0])}
        self.falseNegative = {key: value for (key, value) in zip(self.words,[0,0,0,0,0,0])}
        self.falsePositive = {key: value for (key, value) in zip(self.words,[0,0,0,0,0,0])}
        
        # for decision making
        self.stategy= None
        self.myActionHistory=[]
        self.setupAi=False

        # TD learning
        self.TDTrainingData = []
        self.TDStateDict = {}
        
        # action strategy we have
        # self.actions=["red_heart","green_heart","flag","escape","clearing","shortest","worth"]
        self.actions=["red_heart","green_heart","flag","clearing","shortest","worth","aggressive"]
        
    def traslateOutputBinaryToWord(self, result):
        return self.words[result]

    def predict(self, feature, target):

        # Predit by NEAT
        # predic_result=self.getPredictResultNEAT(feature)

        # Predit by NeuralNetwork
        predic_result = self.getPredictResultNeural(feature)

        # traslate [1 0 0 0 0 0] ->1
        targetx = np.argmax(target)

        # traslate 1 -> "left"
        predic_result = self.traslateOutputBinaryToWord(predic_result)

        targetx = self.traslateOutputBinaryToWord(targetx)

        # save temp static in game to report after end game and print predict result
        self.updateResultPredict(predic_result, targetx)

    def getPredictResultNeural(self, feature):
        
        network = self.brain

        # with PCA
        # output=network.predict(feature.reshape(1,-1))
        # with out PCA
        output = network.predict(np.array(feature).reshape(1, -1))

        return output[0]

    def getPredictResultNEAT(self, feature):

        # set network to last champions
        network = self.brain.champions[-1]
        
        if not isinstance(network, NeuralNetwork):
            network = NeuralNetwork(network)
        
        # feed forward network
        network.make_feedforward()

        # Feed with bias
        output = network.feed(feature)

        # Grab the output
        output = output[-6:]

        # clip output with softmax
        output = np.exp(output) / np.sum(np.exp(output), axis=0)

        # after this part is the predict helper part -> which use to improve prediction from ANN
        show_output = list(output)
        
        cMax = max(show_output)
        max_list = []

        for (index, x) in enumerate(show_output):
            
            if x == cMax and cMax != 0:
                max_list.append(index)
            
            if x < 0.1e-10:
                show_output[index] = 0

        if len(max_list) > 1:
            # random choose
            outputx = random.choice(max_list)
        elif len(max_list) == 0:
            outputx = None
        else:
            outputx = max_list[-1]
        
        return outputx

    def updateResultPredict(self, predic_result, real_result):

        # collect predict result
        if predic_result == real_result:
            self.predict_true += 1
            self.truePositive[predic_result]+=1

        if predic_result != real_result:
            self.predict_false += 1
            self.falsePositive[predic_result]+=1
            self.falseNegative[real_result]+=1
                
        # print result
        print 'predic:', predic_result, '     real_result:', real_result

    def showPredictStat(self):

        sample_number = self.predict_false + self.predict_true

        if sample_number > 0:
            print "-----Overall--------"
            print 'Sample:', sample_number
            print 'True:', self.predict_true
            print 'False:', self.predict_false
            
            percent = self.predict_true / sample_number * 100
            print 'Prediction Percent:', percent

            if os.path.isfile(G('save-path-static-predictor') + self.environment.username + '_predictor.json'):
                f = open(G('save-path-static-predictor') + self.environment.username + '_predictor.json', 'r')
                x = json.load(f)
                f.close()
                f = open(G('save-path-static-predictor') + self.environment.username + '_predictor.json', 'w')
                
                x['cycle'] += 1
                x['true'] += self.predict_true
                x['false'] += self.predict_false
                x['sample'] += sample_number
                x['percent'] = x['true'] / x['sample'] * 100
                # x['total_train_time']+=self.train_time
                
                f.write(json.dumps(x))
                f.close()

            else:
                f = open(G('save-path-static-predictor') + self.environment.username + '_predictor.json', 'w')
                
                x = {}
                x['cycle'] = 1
                x['true'] = self.predict_true
                x['false'] = self.predict_false
                x['sample'] = sample_number
                x['percent'] = x['true'] / x['sample'] * 100
                # x['total_train_time']=self.train_time

                f.write(json.dumps(x))
                f.close()
            
            for word in self.words:
                
                print "--",word,"--"

                precision=(float(self.truePositive[word])/(self.truePositive[word]+self.falsePositive[word]))*100
                recall=(float(self.truePositive[word])/(self.truePositive[word]+self.falseNegative[word]))*100
                
                print "Precision(%s/%s):%s"%(self.truePositive[word],(self.truePositive[word]+self.falsePositive[word]),precision)
                print "Recall(%s/%s):%s"%(self.truePositive[word],(self.truePositive[word]+self.falseNegative[word]),recall)
                print "-----------------"
            
            print "--Not wait--"

            tp=0
            fp=0
            fn=0

            for word in self.words:
                if word != 'wait':
                    tp+=self.truePositive[word]
                    fp+=self.falsePositive[word]

            fn=self.falsePositive['wait']        
            precision=(float(tp)/(tp+fp))*100
            recall=(float(tp)/(tp+fn))*100

            print "Precision(%s/%s):%s"%(tp,(tp+fp),precision)
            print "Recall(%s/%s):%s"%(tp,(tp+fn),recall)
            print "-----------------" 
            
            
    def trainNEAT(self, features, outputs):
        
        task = predictorTask(True, features, outputs)

        # task with out pca
        # task = predictorTask(True,self.input_mem,self.output_mem)

        nodecounts = defaultdict(int)

        # if we already have save
        if self.brain is not None:
            # get population
            pop = self.brain
            
            # run round of epoch
            for i in xrange(1):

                # Run the evolution, tell it to use the task as an evaluator
                pop.epoch(generations=G('num-generation'), evaluator=task, solution=task, reset=False)
                nodecounts[len(pop.champions[-1].node_genes)] += 1

        # if we do not have save yet
        else:
            # new genotype function for new brain -> NOW -> need 3 type -> input , hidden , output
            genotype = lambda : NEATGenotype(inputs=len(features[0]), outputs=6, weight_range=(-3., 3.), types=['sigmoid', 'sigmoid', 'sigmoid'])

            # Create a population with new genotype
            pop = NEATPopulation(genotype, popsize=G('num-pop'))
            
            for i in xrange(1):

                # Run the evolution, tell it to use the task as an evaluator
                pop.epoch(generations=G('num-generation'), evaluator=task, solution=task, reset=True) 
                nodecounts[len(pop.champions[-1].node_genes)] += 1

        # print sorted(nodecounts.items())

        return pop
    
    def trainPredictorAI(self, features, outputs):

        # Create a factory for genotypes (i.e. a function that returns a new instance each time it is called)

        if len(self.input_mem) <= 50:
            self.environment.restartGame()
            return

        start_time = time.time()

        # Train NETWORK with NEAT
        # network=self.trainNEAT(features,outputs)

        # Train NETWORK with NEAT
        network = self.trainNeural(features, outputs)

        # Show time to Train
        self.train_time = time.time() - start_time
        print '--- %s seconds ---' % self.train_time

        # Save network
        self.saveToFile(G('save-path-brain'), self.environment.username + '_network.json', 'obj', network)

        # Auto restart with fix cycles
        # if G('simulation-auto-restart-after-learn') and G('cycle-simulation') > self.environment.cycle_game:
            # self.environment.restartGame()

    def trainNeural(self, features, outputs):

        # translate [1 0 0 0 0 0 ] -> 1
        outputs = np.argmax(outputs, axis=1)
        print outputs

        # train NETWORK with NeuralNetwork
        network = OneVsRestClassifier(LinearSVC(random_state=0))
        network.fit(features, outputs)
       
        print "score:",network.score(features, outputs)

        return network

    
    def getFeature(self, environment):

        standard_input = [0] * 5
        environment_input = environment.getFeature()

        # [playerPosX,playerPosY,aiPosX,aiPosY,playerHp,aiHp,playerPower,aiPower,flag  -> 9
        # 33,34,35,36,37,38,39,310,311,312,313,314,315 -> 13 * 13 end at 1515
        # 9 + 13*13 = 178

        if len(environment_input) > 0:

            for (key, value) in environment_input:

                # static feature
                if key == 'player_life':
                    standard_input[0] = value

                if key == 'ai_life':
                    standard_input[1] = value

                if key == 'player_power':
                    standard_input[2] = value

                if key == 'ai_power':
                    standard_input[3] = value

                if key == 'flag_status':
                    standard_input[4] = value
                        
            # standard_input.append(environment.board._total_time)
            
            # All PCA Way
            # board_input = environment.getBoardDetail(environment_input)
            # standard_input.extend(board_input)

            # Separate PCA Way
            bomb_input = self.environment.getBoardBomb(environment_input)

            redHeard_input = self.environment.getBoardRedHeart(environment_input)

            greenHeart_input = self.environment.getBoardGreenHeart(environment_input)

            flag_input = environment.getBoardFlag(environment_input)

            block_input = self.environment.getBoardBlock(environment_input)

            boom_input = self.environment.getBoardBoom(environment_input)

            player_position = self.environment.getBoardPlayer(environment_input)

            ai_position = self.environment.getBoardAi(environment_input)

            standard_input.extend(bomb_input + redHeard_input + greenHeart_input + block_input + boom_input 
                                + player_position + ai_position + flag_input)

            return standard_input

    def guessPlayerAction(self, board):

        (ex, ey) = board.getPosition(self.enemy)

        oex = self.enemy_position['enemy_x']
        oey = self.enemy_position['enemy_y']
        
        guess = (0, 0, 0, 0, 0, 0)

        enemy_bomb = board.getLocationsWithManOfType(bomb.Bomb)

        # left:1 right:2 up:3 down:4 wait:0 bomb:5

        if oex > ex and oey == ey:
            guess = (1, 0, 0, 0, 0, 0)
            # guess = "left"

        if oex < ex and oey == ey:
            guess = (0, 1, 0, 0, 0, 0)
            # guess = "right"

        if oey > ey and oex == ex:
            guess = (0, 0, 1, 0, 0, 0)
            # guess = "up"

        if oey < ey and oex == ex:
            guess = (0, 0, 0, 1, 0, 0)
            # guess = "down"

        if oex == ex and oey == ey:
            guess = (0, 0, 0, 0, 0, 1)
            # guess = "wait"

            for (bx, by) in enemy_bomb:
                if bx == oex and by == oey:
                    guess = (0, 0, 0, 0, 1, 0)
                    # guess= "bomb"
                    break

        self.enemy_position['enemy_x'] = ex
        self.enemy_position['enemy_y'] = ey

        return guess

    
    def saveToFile(self, path, namefile, type_data, data):

        f = open(path + namefile, 'w')

        if type_data == 'obj':
            pickle.dump(data, f)
        if type_data == 'json':
            f.write(json.dumps(data))
            # f.write(data)

        f.close()

    def loadFromFile(self, path, namefile, type_data):

        if os.path.isfile(path + namefile):
            
            f = open(path + namefile, 'r')
            
            if type_data == 'obj':
                x = pickle.load(f)
            if type_data == 'json':
                x = json.load(f)
            
            f.close()
            
            return x

        else:
            
            return None

    def saveHistory(self):

        # No PCA -> Load History log
        history = self.loadFromFile(G('save-path-history'), self.environment.username + '.json', 'json')

        # print history
        if history is not None:

            # Decode unzip zip data
            history = zip(*history)

            # Decode array of array to array of tuple
            history_input = map(tuple, history[0]) + self.input_mem
            history_output = map(tuple, history[1]) + self.output_mem

        else:

            history_input = self.input_mem
            history_output = self.output_mem

        # Add Last History
        history = zip(history_input, history_output)

        # Save History log
        self.saveToFile(G('save-path-history'), self.environment.username + '.json', 'json', history)

        return (history_input, history_output)
    
    
    def onEndangered(self, event):
        """We were endangered"""
        self.log.debug('%s is endangered - escaping' % event.man.getNiceName())
        self.chooseEscapeDestination(event.man, event.board)
        self.state.current = S_ESCAPING
    
    def onArrived(self, event):
        """We arrived at our destination"""

        self.log.debug('%s arrived at destination' % event.man.getNiceName())
        
        # if event.src in (S_MOVING, S_CLEARING):
        if event.src in (S_CLEARING):
            self.tryToDropBomb(event.man, event.board)
        
        # elif event.src == S_ESCAPING:
        # When we arrive at the escape square then we should wait for quite a bit to allow the bomb to go off
            # self._last_move -= G('ai-wait-cycles') * self.move_interval
    
    
    
    def findShortest(self,(mx,my),possible,board,exceptList):
        
        paths=[]
        
        for strategy, x, y in possible:

            pass_it=False
            
            for ex,ey in exceptList:
                if (x,y) == (ex,ey):
                    pass_it=True
                    break
                
            if not pass_it:
                try:
                    path = board.getPath((mx, my), (x, y))
                    paths.append((len(path), path,"restricted"))
                except NoPath:
                    # Ok - there is no path
                    path = board.getPath((mx, my), (x, y),unrestricted=True)
                    #pass
                    paths.append((len(path)+5, path,"unrestricted"))
        
        if paths:
                paths.sort(lambda a, b: cmp(a[0], b[0]))
                return paths[0]
        else:
            return False
    
    def makeNextMove(self, man, board):
        """Make the next move to our destination"""
        
        mx, my = board.getPosition(man)
       
        event = serge.blocks.settings.Bag()
        event.man = man
        event.board = board
        
        if not self.selected_path :

            #print "state(0)",self.state.current
            
            if self.state.current!=S_WAITING:
                self.state.arrived(man=man, board=board)
            else:
                if not board.isSafe(board.getPosition(man)):
                    if self.state.current != S_ESCAPING:
                        self.onEndangered(event)
                    self.state.endangered(man=man, board=board)
                else:
                    self.state.current = S_WAITING
        
        else:
            
            #print "state(1)",self.state.current
            
            nx, ny = self.selected_path[0]
            
            # Only make the move if we can
            if not board.canOccupy(man, (nx, ny)):

                self.log.debug('%s cannot move to %s, %s' % (man.getNiceName(), nx, ny))
                
                if board.isSafe((nx, ny)):
                    self.state.current = S_WAITING
                    
                    if board.getOverlapping((nx, ny))[0].__class__.__name__ == "Man":
                        self.tryToDropBomb(event.man, event.board)  
                        # print "man in"
                    # print "++++++++++++++++++++++++++++++++++",board.getOverlapping((nx, ny))[0].__class__.__name__
                
                else:
                    if self.state != S_ESCAPING:
                        self.onEndangered(event)
                    self.state.endangered(man=man, board=board)
            
            elif board.isSafe((nx, ny)) or (self.state.current == S_ESCAPING and not board.isDeadly((nx, ny))):
                # print '2'
                # print mx,my
                # print board.isSafe((nx, ny))
                
                # Only if it is safe or if we are escaping
                # if not self.walk.isPlaying():
                    # self.walk.play()
                
                board.moveMan(man, (nx - mx, ny - my))

                self.environment.saveDataTrain(self.environment.getFeature(),(nx - mx, ny - my))
                
                self.selected_path.pop(0)
            
            else:
                #print "3"
                
                # Ok - it isn't safe to make the move
                
                self.log.debug('Not safe for %s to move to %s, %s' % (man.getNiceName(), nx, ny))
                
                if not board.isSafe((mx, my)):
                    if self.state.current != S_ESCAPING:
                        self.onEndangered(event)
                    self.state.endangered(man=man, board=board)
                
                else:
                    #print "4"
                    self.state.current = S_WAITING

    def tryToDropBomb(self, man, board):
        """Try to drop a bomb"""

        if self.isSafeToDropBomb(man, board):
            self.makeMove('b', man, board)
            serge.sound.Sounds.play('drop')
            self.state.drop_bomb()
            self.chooseEscapeDestination(man, board)
        
        else:
            event = serge.blocks.settings.Bag()
            event.man = man
            event.board = board
            if not board.isSafe(board.getPosition(man)):
                if self.state.current != S_ESCAPING:
                    self.onEndangered(event)
                self.state.endangered(man=man, board=board)
            else:
                self.state.current = S_WAITING
            self.log.info('Not safe for %s to drop bomb' % man.getNiceName())
    
    def makeMove(self, move, man, board):
        
        """Make a move"""
        if move == ' ':
            pass
        elif move == 'b':
            board.dropBomb(man)
        else:
            direction = serge.blocks.directions.getVectorFromCardinal(move)
            board.moveMan(man, direction)
    
    
    def canDestroyEnemy(self,man,board):
        try:
            # player location
            mx, my = board.getPosition(man)
            # ai location
            ex, ey = board.getPosition(self.enemy)
            bomb_distance=man.bomb_distance
        except:
            return False
        try:
            path_distance=len(board.getPath((mx,my),(ex,ey)))
        except NoPath:
            return False        
                      
        if mx==ex and abs(my-ey)<=bomb_distance and len(board.getPath((mx,my),(ex,ey)))==abs(my-ey):
            return True
        elif my==ey and abs(mx-ex)<=bomb_distance and len(board.getPath((mx,my),(ex,ey)))==abs(mx-ex):
            return True
        return False
    
    def isSafeToDropBomb(self, man, board):
        """Check if it is safe to drop a bomb at the current time"""
        
        # It is safe if we can get to a location which is safe beyond the blast radius of our bomb
        x, y = board.getPosition(man)
        blast_radius = man.bomb_distance
        
        max_search = 2 * blast_radius

        for _, (sx, sy) in board.breadthFirstDestinationSearch(man, (x, y)):

            if (sx != x and sy != y) and board.isSafe((sx, sy)) and board.pathIsSafe(board.getPath((x,y),(sx,sy))):
                # Ok, out of line of fire of this bomb and safe
                return True
            
            elif (abs(sx - x) > blast_radius or abs(sy - y) > blast_radius) and board.isSafe((sx, sy)) and board.pathIsSafe(board.getPath((x,y),(sx,sy))):
                # Ok, outside of the blast radius
                return True
            
            elif abs(sx - x) + abs(sy - y) > max_search:
                # Nothing close to us
                return False
        else:
            
            # No path to safety
            return False
        
    def makeAction(self, move):
        
        """Make a move"""
        mx, my = self.environment.board.getPosition(self.environment.ai)

        direction = None
        
        if move == 'bomb':
            self.environment.board.dropBomb(self.environment.ai)
        
        if move == 'wait':
            return False
        
        if move == 'left':
            direction = (-1, 0)

        elif move == 'right':
            direction = (+1, 0)

        elif move == 'up':
            direction = (0, -1)

        elif move == 'down':
            direction = (0, +1)

        if direction is not None :   
            if self.environment.board.canMove(self.environment.ai,direction):
                self.environment.board.moveMan(self.environment.ai, direction)
            else:
                return False
            
        return True
    
    def chooseDestination(self, man, board,enviroment,action):
        """Choose the next destination"""

        self.log.debug('Looking for possible attack locations')
        
        # Pick attack locations
        
        # player location
        mx, my = board.getPosition(man)
        # ai location
        ex, ey = board.getPosition(self.enemy)
        
        if action == "escape":
            self.chooseEscapeDestination(man,board)
            return True
        
        else :
            
            possible = []
            
            if action == "red_heart" or action == "shortest" or action =="worth":
                possible.extend([("red_heart", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.RedHeart)])
            
            if action =="green_heart" or action == "shortest" or action =="worth":
                temp=[]
                temp.extend([("red_heart", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.RedHeart)])
                possible.extend([("green_heart",nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.Heart) if (("red_heart",(nx,ny))) not in temp ])
            
            if action == "flag" or action == "shortest" or action =="worth":
                possible.extend([("flag", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.Flag)])
            
            if action == "clearing" :
                possible.extend([("block",x, y) for x in range(19) for y in range(19) if self.board.canDestroy((x, y)) ] )
            
            if action =="aggressive":
                if self.canDestroyEnemy(man,board):
                    self.tryToDropBomb(man,board)
                    return True
                else :
                    possible.append(("enemy",ex,ey))
            # for dx, dy in self.strategy_offsets[self.current_strategy]:
            #     nx, ny = ex + dx, ey + dy
            #     if board.canOccupy(man, (nx, ny)):
            #         possible.append((X_PLAYER, nx, ny))
            #
            
            # Find the closest one we can get to
            paths = []
            
            for strategy, x, y in possible:

                if (mx, my) == (x, y):
                    continue
                
                try:
                    path = board.getPath((mx, my), (x, y))
                    paths.append((len(path), path,"restricted"))
                
                except NoPath:
                    # Ok - there is no path
                    path = board.getPath((mx, my), (x, y),unrestricted=True)
                    #Pass
                    paths.append((len(path)+5, path,"unrestricted"))
            
            distant=[]
            
            # Find shortest, if there is one
            if paths:
                #print "have path"
                paths.sort(lambda a, b: cmp(a[0], b[0]))

                if action == "worth":
                    if len(paths)>=3:
                        
                        for i in range(0,3):
                            try:
                                distant.append(paths[i])
                                next_path=paths[i]
                                exceptList=[]
                                exceptList.append(next_path[1][-1])
                                
                                for j in range(0,4):
                                    
                                    next_path=self.findShortest(next_path[1][-1],possible,board,exceptList)
                                    if not next_path:
                                        break

                                    exceptList.append(next_path[1][-1])
                                    
                                    if next_path[0]<10:
                                        distant[i] = list(distant[i])
                                        distant[i][0]=(distant[i][0]*(j+1)+next_path[0])/(j+2)
                                        distant[i] = tuple(distant[i])
                                    else:
                                        break

                            except :
                                break

                        distant.sort(lambda a, b: cmp(a[0], b[0]))
                        paths=distant

                if paths[0][2] == "restricted":
                    self.selected_path = paths[0][1][1:]
                    self.state.current = S_MOVING
                
                else:
                    # No paths so find a clearing path
                    self.chooseClearingDestination(man, board,paths[0][1])
                
                return True
            
            else:
                #print "no path"
                self.state.current = S_WAITING
            
            return True
    
    def chooseEscapeDestination(self, man, board):
        """Choose a location to escape to"""
        
        # From the current square - do a breadth first
        # search of the places that we can get to. Choose the path that is safe
        x, y = board.getPosition(man)

        for _, (dx, dy) in board.breadthFirstDestinationSearch(man, (x, y)):
            if board.isSafe((dx, dy)):
                self.log.debug('Found escape square %s, %s for %s' % (dx, dy, man.getNiceName()))
                break
        else:
            # No place to go - just pick the last anyway
            self.log.debug('No escape square for %s' % man.getNiceName())
            self.selected_path = []
            return
        
        path = board.getPath((x, y), (dx, dy))
        self.selected_path = path[1:]
        #print self.selected_path
    
    def chooseClearingDestination(self, man, board,path):
        """Choose a destination to drop a bomb to clear a way"""
        self.log.debug('Looking for clearing path for %s' % man.getNiceName())
        
        # Find the best path from us to the enemy
        # x, y = board.getPosition(man)
        # ex, ey = board.getPosition(self.enemy)
        # try:
        #     path = board.getPath((x, y), (ex, ey), unrestricted=True)
        # except NoPath:
        #     path = None
        
        # Make sure we have a path
        if not path:
            self.log.debug('No path found')
            return
        
        # Look for the bombing location
        chosen_path = []
        
        for (px, py) in path[1:]:
            if board.isDestructible((px, py)):
                self.log.debug('Found path')
                break

            chosen_path.append((px, py))
            
        else:
            
            # This probably means that a block was already destroyed
            # so we should just wait for another cycle
            self.log.debug('No path with destructible found')
            # print "else"
            return
        
        # print chosen_path
        self.state.current = S_CLEARING
        self.selected_path = chosen_path
    
    
    
    
    
    def getState(self):
        return ''.join(str(x) for x in self.environment.getBoardDetail(self.environment.getFeature()))
    
    def getState(self,environment,board):
        
        #How many heart you have
        player_hearts=environment.player_hearts.getValue('hearts')
        
        #How manu heart enemy have
        ai_hearts=environment.ai_hearts.getValue('hearts')

        #how many flag
        possible=[]
        possible.extend([("flag", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.Flag)])
        num_flag=len(possible)

        #How many red_heart
        possible=[]
        possible.extend([("red_heart", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.RedHeart)])
        num_red_heart=len(possible)
        
        #HOW many green_heart
        temp=[]
        possible=[]
        temp.extend([("red_heart", nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.RedHeart)])
        possible.extend([("green_heart",nx, ny) for nx, ny in board.getLocationsWithManOfType(powerups.Heart) if (("red_heart",(nx,ny))) not in temp ])
        num_green_heart=len(possible)
        
        # status player heart is high medium or low   
        if player_hearts>5:
            player_hearts_status=5
        else :
            player_hearts_status=player_hearts

        # status ai heart is high medium or low   
        if ai_hearts>5:
            ai_hearts_status=5
        else :
            ai_hearts_status=ai_hearts

        #status flag(print du noi)
        if self.environment.flag_status_panel.flag_position==0:
            flag_status=2
        elif self.environment.flag_status_panel.flag_position>0:
            flag_status=3
        elif self.environment.flag_status_panel.flag_position<0:
            flag_status=1
        
        #Near Player
        try:
            mx, my = board.getPosition(environment.player)
            ex, ey = board.getPosition(environment.ai)
        except:
            near_enemy=0
        else:
            try:
                aiToPlayerPath=board.getPath((mx,my),(ex,ey))
            except NoPath:
                aiToPlayerPath=[]
            if len(aiToPlayerPath)<6:
                near_enemy=1
            else:
                near_enemy=0

        #Has red_heart
        has_red_heart=1 if num_red_heart else 0
        #Has green_heart
        has_green_heart=1 if num_green_heart else 0
        #Has flag
        has_flag=1 if num_flag else 0

        #Can kill buy red heart
        can_kill_by_red_heart=1 if num_red_heart-player_hearts>=0 else 0
        #Can die buy red heart
        can_die_by_red_heart=1 if num_red_heart-ai_hearts>=0 else 0
        
        #collect available_path value
        px, py = self.board.getPosition(self.enemy)
        ax, ay = self.board.getPosition(self.man)
        player_path = self.board.getArea(self.enemy,self.board)
        ai_path = self.board.getArea(self.man,self.board)

        #player_path level
        if player_path>20:
            player_path_status=5
        elif player_path>=15 and player_path<=20:
            player_path_status=4
        elif player_path>=10 and player_path<15:
            player_path_status=3
        elif player_path>=5 and player_path<10:
            player_path_status=2
        else :
            player_path_status=1

        #ai_path level
        if ai_path>20:
            ai_path_status=5
        elif ai_path>=15 and ai_path<=20:
            ai_path_status=4
        elif ai_path>=10 and ai_path<15:
            ai_path_status=3
        elif ai_path>=5 and ai_path<10:
            ai_path_status=2
        else :
            ai_path_status=1

        #collect power value 
#        player_power = self.environment.PLAYER_Explosion_Distance
#        ai_power = self.environment.AI_Explosion_Distance
#
#        #player power level
#        if player_power>4:
#            player_power_status=3
#        elif player_power>=3 and player_power<=4:
#            player_power_status=2
#        else:
#            player_power_status=1
#
#        if ai_power>4:
#            ai_power_status=3
#        elif ai_power>=3 and ai_power<=4:
#            ai_power_status=2
#        else:
#            ai_power_status=1

        #state for reward    
  
        return ''.join(str(x) for x in [player_hearts_status,ai_hearts_status,has_red_heart,has_green_heart,has_flag,
            can_kill_by_red_heart,can_die_by_red_heart,flag_status,player_path_status,ai_path_status,near_enemy])
                                      
    def getActionReward(self,state,action,term,debug=0):

        """[player_hearts_status, 
            ai_hearts_status,
            has_red_heart,
            has_green_heart
            has_flag,
            can_kill_by_red_heart,
            can_die_by_red_heart,
            flag_status,
            player_path_status,
            ai_path_status,
            player_power_status,
            ai_power_status]"""
        
        reward = 0
        
        #break down state number to list 
        state_list = [int(i) for i in str(state)]
        
        #define factor
        avai_path_factor = 0.3
        power_factor = 0.1
        heart_factor = 0.7
        flag_factor = 1.5

        #avai_path ok
        if 'available_path' in term:
            player_avai_path = state_list[8]
            ai_avai_path = state_list[9]
            dif_avai_path = ai_avai_path - player_avai_path
            reward = reward + (avai_path_factor * dif_avai_path) #0.1*(1<-->5)=(0.1--0.5)

        #heart need fix
        if 'heart' in term:
            p_heart =  state_list[0]
            a_heart = state_list[1]
            reward = reward + (heart_factor * (5-p_heart)) + (heart_factor * (a_heart - 5 ))
            #0.5*(5-(1--3)+)
            #reward = reward + heart_factor * (a_heart - p_heart )
        #flag need fix
        if 'flag_status' in term:
            flag_status = state_list[7] - 2
            reward = reward + (flag_factor * flag_status )
            
        #power can remove
#        if 'power' in term:
#            p_power = state_list[10]
#            a_power = state_list[11]
#            dif_power = a_power - p_power
#            reward = reward + (power_factor * dif_power)

        #print for debug(TRUE)
        if debug==2:
            print 'State number',state
            
            if 'avai_path' in term:
                print "Dif Available Path :",dif_avai_path
            
            if 'heart' in term:
                print 'Player Heart :',p_heart
                print 'AI Heart :',a_heart
            
            if 'flag_status' in term:
                print 'Flag Status :',flag_status
            
#            if 'power' in term:
#                print 'Dif Power :',dif_power
            
            print "State' Reward :",reward
            print '------------------------'   
        
        if debug==1:
            print 'State Reward :',reward
        
        return reward
        
    
    def reinforcementMdpDecisionMaking(self,state,typeAction,typeSave,allAction):


        action=G('ai-player-strategy')

        result=self.chooseDestination(self.man,self.board,self.environment,action)   
        
        if result:
            return True  
        else:
            return False

    def reinforceMdpTraining(self,actionHistory,rewards):


        gamma=0.9
#        obs1 = [[u"A",u"F"],[u"A",u"L"],[u"Prize","uF"]]
#        obs2 = [[u"C",u"R"],[u"D",u"F"],[u"B",u"B"],[u"D",u"L"]]
#        obs3 = [[u"C",u"F"],[u"A",u"R"],[u"B",u"L"],[u"A",u"L"],[u"Prize",u"L"]]
#        actionHistory = [obs1,obs2,obs3]
#        rewards = [0,0,1]
#        print actionHistory
#        print rewards

        if G("save-reward-type")=="overall":
            model = l.learn(actionHistory,gamma,rewards)
        else:
            model = l.learn(actionHistory,gamma)
        self.saveToFile(G('save-path-model'),self.environment.username+".json","json",model)
            
        
    # def reinforceMdpTraining(self,actionHistory):
    #     gamma=1
    #     obs1 = [[u"A",u"F"],[u"A",u"L"],[u"Prize","uF"]]
    #     obs2 = [[u"C",u"R"],[u"D",u"F"],[u"B",u"B"],[u"D",u"L"]]
    #     obs3 = [[u"C",u"F"],[u"A",u"R"],[u"B",u"L"],[u"A",u"L"],[u"Prize",u"L"]]
    #     actionHistory = [obs1,obs2,obs3]
    #     rewards = [0,0,1]
    #     print actionHistory
    #     print rewards
    #     count =0
    #     for i in range(len(actionHistory)):
    #         count+=len(actionHistory[i])
    #     print count
    #     start_time = time.time()
    #     model = l.learn(actionHistory,gamma)
    #     train_time = time.time() - start_time
    #     print '++ %s seconds ++' % train_time
    #     self.saveToFile(G('save-path-model'),self.environment.username+".json","json",model)

    
    def collectingTDData(self,state,action):
        
        actionNum = 0

        # add new state_dict if its a new state 
        if state not in self.TDStateDict:
            self.TDStateDict[state] = [0] * len(self.actions)

        # find action number according to available action and append for training
        for index,act in enumerate(self.actions):
            if act == action:
                self.TDtrainingData.append((state,index))

    def TDTraining(self,learningType):
        
        # format [state:action,state:action.......]

        lastState = None
        lastAction = None
        nextState = None
        nextAction = None
            
        for state,actionNum in self.TDtrainingData:
            
            if lastState == None:
                lastState = state
                lastAction = actionNum
                continue
            
            nextState = state
            nextAction = actionNum
            
            determine_term = ['available_path','heart','flag_status','power']
            reward = self.getActionReward(nextState,lastAction,determine_term,debug=2)
            
            if learningType == 'q_learning':
                self.updateQFunction(lastState,nextState,lastAction,reward)
            elif learningType == 'sarsa':
                self.updateSarsaFunction(lastState,nextState,lastAction,nextAction,reward)
            
            lastState = nextState
            lastAction = nextAction

    def updateQFunction(self,lastState,nextState,lastAction,reward):
        
        # define factor
        alpha = 0.3
        discount = 0.6

        # q-function
        self.TDStateDict[lastState][lastAction] = (self.TDStateDict[lastState][lastAction] + 
                (alpha * (reward + (discount * max(self.TDStateDict[nextState])) - self.TDStateDict[lastState][lastAction])))

    def updateSarsaFunction(self,lastState,nextState,lastAction,nextAction,reward):

        # define factor
        alpha= 0.3
        discount = 0.6

        # sarsa-function
        self.TDStateDict[lastState][lastAction] = (self.TDStateDict[lasteState][lastAction] + 
                (alpha * (reward + (discount * self.TDStateDict[nextState][nextAction]) - self.TDStateDict[lastState][lastAction])))

    def TDChoosingActionPolicy(self,choosingType,state):

        # e-greedy , e-soft , softmax
        playRound = 1
        epsilon = 1.0/playRound

        # random choose
        if random.random()<epsilon:
            choosedAction = random.choice(self.actions)

        # choose the best
        else:
            posAct = []
            maxAct = max(self.TDStateDict[state])
            for index,act in enumerate(self.actions):
                if self.TDStateDict[state][index] == maxAct:
                    posAct.append(act)
            choosedAction = random.choice(posAct)

        return choosedAction



    
    
    def updateController(self, interval, man, board, environment):
        """Update the control of the actor"""
        
        self.man=man
        self.environment = environment
        self.board=board
        
        #print environment.board.getSquaresAround(man,5)
        
        self._last_move += interval / 1000.0
        #self._total_time += interval / 1000.0
        self._total_time = board._total_time
        
        if self._last_move > self.move_interval :

            if self.state.current != S_ESCAPING:
                if self.reinforcementMdpDecisionMaking(self.getState(environment,board),"stategy",G("save-reward-type"),self.actions) :
                    self.makeNextMove(man, board)
            else :
                self.makeNextMove(man, board)

            self._last_move = 0.0

class AIUI(serge.actor.Actor):

    """A user interface to report on the state of the AI"""

    def __init__(self, controller, board, name):
        """Initialise the ui"""

        super(AIUI, self).__init__('debug', name)

        #
        self.controller = controller
        self.board = board

    def addedToWorld(self, world):
        """Added to the world"""

        super(AIUI, self).addedToWorld(world)

        
        # The debugging text
        self.text = serge.blocks.utils.addTextToWorld(world, 'AIUI', self.name, theme, 'ui')
        self.text.tag = 'debug'

        # The destination square
        self.destination = serge.blocks.utils.addVisualActorToWorld(world, 'debug', 'desintation',
                serge.blocks.visualblocks.Rectangle(G('board-cell-size'), G('%s-destination-colour' % self.name)), 'ui')
        self.destination.visible = False
        self.destination.active = G('ai-show-destinations')
    
    def updateActor(self, interval, world):
        """Update the actor"""

        super(AIUI, self).updateActor(interval, world)

        # Update debug text
        self.text.visual.setText('''Strategy: %s
State: %s
Dest: %s
Length: %s'''
                                 % (self.controller.current_strategy,
                                 self.controller.state.current, ('n/a'
                                  if not self.controller.selected_path else self.controller.selected_path[-1]),
                                 ('n/a'
                                  if not self.controller.selected_path else len(self.controller.selected_path))))

        # Update destination square
        if self.controller.selected_path:
            self.destination.visible = True
            self.destination.moveTo(*self.board.screenLocation(self.controller.selected_path[-1]))
