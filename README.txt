READ ME

- - - - - Installation - - - - -

	1.install python2.7

	2.install pygame

	3.install pip
		"python get-pip.py"

	4.install other module
		numpy, scipy, reinforce-master
		"python -install setup.py"

	5.install scikit
		"pip install -U scikit-learn"





- - - - - Set up Theme - - - - -
	1.use MDP
		TD-Learning-On = False
		MDP-Learning-On = True
		save-MDP-action-on = True
		ai-decision = 'mdp'

	2.use TD (q-learning,Sarsa)
		TD-Learning-On = True
		TD-Learning-Method = 'q_learning' or 'sarsa'
		MDP-Learning-On = False
		save-MDP-action-on = False
		ai-decision = 'td'

	3.Open Simulation
		simulation-on = True
		simulation-auto-restate = True

	4.AI as Player 
		ai-player = True
		ai-player-strategy = 'origin' or etc

	5. Person as PLayer
		ai-player = False

	6. No Learning Test
		explorer-point = 0
		MDP-Learning-On = False
		save-MDP-action-on = False
		TD-Learning-On = False





- - - - - Save Path - - - - -

	1.static = player static
	2.brain = ann for NEAT (not use)
	3.pca = pca pane save file (not use)
	4.session = session (needed)
	5.predictor_static = predict static (not use)
	6.history = action history for predictor (not use)
	7.mdp/action = mdp action history
	8.mdp/model = mdp policy model
	9.TD = TD statedict



- - - - - Importent File - - - - -

	1. ai.py -> bombr-0.3/game/
	2. mainscreen.py -> bombr-0.3/game/
	3. theme.py -> bombr-0.3/game/
	4. all save file -> bombr-0.3/save/




- - - - - Run the Game - - - - -
	
	1. open command line on bombr-0.3 folder
	2. type 'python bombr.py'
	3. can close the game with ctrl+c